class BadRequestError extends Error {
    constructor(message) {
        super(message);
        this.error = 'BadRequestError';
    };
}
class NotFoundError extends Error {
    constructor(message) {
        super(message);
        this.error = 'NotFoundError';
    }
}
module.exports = {
    NotFoundError,
    BadRequestError
};