import {Injectable, OnDestroy, OnInit} from '@angular/core';
import {RequestsService} from '../requests/requests.service';
import * as moment from 'moment-business-days';
import {Subject, Subscription} from 'rxjs';
import {Moment} from 'moment';
import { DialogComponent } from '../header/dialog.component';
import { MatDialog } from '@angular/material';

const format = 'DD-MM-YYYY';

@Injectable({
  providedIn: 'root'
})
export class CalendarService implements OnInit, OnDestroy {
  currentDate: Moment;
  firstDay: Moment;
  currentSeason: Number;
  cardNum: Number = 0;
  public changeDateSubject = new Subject<any>();
  public changeRangeSubject = new Subject<any>();
  public resetRangeSubject = new Subject<any>();
  public hoverRangeSubject = new Subject<any>();
  changeSeason: Subscription;
  selectedPto;
  rangeCounter = 0;
  equal: any;
  startRange: any;
  endRange: any;

  constructor(private requestsService: RequestsService, private dialog: MatDialog) {
    this.currentSeason = this.requestsService.currentSeason;
    this.currentDate = this.requestsService.currentDate;
    this.firstDay = moment(this.currentDate, format).startOf('month');
    if (this.firstDay.weekday() !== 1) {
      this.firstDay = this.firstDay.startOf('week').add(1, 'days');
    }
    this.changeSeason =  this.requestsService.changeSeasonSubject.subscribe(event => {
      this.currentSeason = event.currentSeason;
    });
  }

  changeWeek(value): void {
    this.firstDay = this.firstDay.add(value, 'days');
    this.changeDateSubject.next();
  }

  jumpToPto(pto): void {
    this.firstDay = moment(pto.fromDate).subtract(14, 'days');
    if (this.firstDay.weekday() !== 1) {
      this.firstDay = this.firstDay.startOf('week').add(1, 'days');
      if (this.firstDay >= moment(pto.fromDate)) {
        this.changeWeek(-7);
      }
    }
    this.selectedPto = pto;
    this.changeDateSubject.next();
  }

  changeCardNum(value, index): void {
    this.cardNum += value;
    if (index === 35) {
      if (this.cardNum >= 18) {
        if (moment(this.firstDay).add(index - 1, 'days') >= this.requestsService.end) {
          this.requestsService.changeSeason({changeValue: 1, changeCalendarStartDate: false});
        } else {
          this.requestsService.changeSeason({changeValue: -1, changeCalendarStartDate: false});
        }
      }
      this.cardNum = 0;
    }
  }
  ngOnInit(): void {
    this.changeSeason = this.requestsService.setPtos.subscribe(event => {
      this.currentSeason = event.currentSeason;
      if (event.failed === true) {
        this.currentSeason = event.currentSeason - event.changeValue;
      }
    });
  }

  rangePicker(day) {
    if(this.rangeCounter % 2 == 0) {
         this.startRange = day;
         this.endRange = day;
         this.rangeCounter++;
         this.changeRangeSubject.next();
    }
    else{
      this.endRange = day
      if(this.endRange == this.startRange){
        this.equal = 1;
      }
      else{
        this.equal = 0;
      }
      this.rangeCounter = 0;
      this.changeRangeSubject.next();
      this.openDialog();
    }
  }

  rangeHovered(day){
    this.hoverRangeSubject.next(day);
  }

  resetRangePicker() {
      this.resetRangeSubject.next();
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(DialogComponent, {
      width: '650px',
      panelClass: 'custom-dialog-container',
      data: {openedRangeSelect: true},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.resetRangeSubject.next();
      dialogRef = null;
    });
  }

  ngOnDestroy(): void {
    this.changeSeason.unsubscribe();
  }
}
