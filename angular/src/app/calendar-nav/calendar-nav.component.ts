import {Component, OnDestroy, OnInit} from '@angular/core';
import {faChevronDown, faChevronUp} from '@fortawesome/free-solid-svg-icons';
import {CalendarService} from '../calendar/calendar.service';
import {RequestsService} from '../requests/requests.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-calendar-nav',
  templateUrl: './calendar-nav.component.html',
  styleUrls: ['./calendar-nav.component.css']
})
export class CalendarNavComponent implements OnInit, OnDestroy {
  faUp = faChevronUp;
  faDown = faChevronDown;
  load = true;
  changeSeason: Subscription;
  setPtos: Subscription;
  constructor(private requestsService: RequestsService, private calendarService: CalendarService) {}

  changeWeek(val) {
    if (this.load) {
      return;
    }
    this.calendarService.changeWeek(val);
    this.calendarService.resetRangePicker();
  }

  ngOnInit() {
    this.changeSeason = this.requestsService.changeSeasonSubject.subscribe(event => {
      this.load = true;
    });
    this.setPtos = this.requestsService.setPtos.subscribe(event => {
      this.load = false;
    });
  }

  ngOnDestroy() {
    this.changeSeason.unsubscribe();
    this.setPtos.unsubscribe();
  }
}
