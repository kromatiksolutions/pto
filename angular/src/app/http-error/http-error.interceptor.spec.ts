import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HttpErrorInterceptor } from './http-error.interceptor';

describe('HttpErrorInterceptor', () => {
  let component: HttpErrorInterceptor;
  let fixture: ComponentFixture<HttpErrorInterceptor>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HttpErrorInterceptor ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HttpErrorInterceptor);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
