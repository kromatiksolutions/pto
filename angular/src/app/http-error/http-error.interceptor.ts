import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpErrorResponse
} from '@angular/common/http';
import {Observable, throwError} from 'rxjs';
import { retry, catchError } from 'rxjs/operators';
import {Component, Inject, Injectable, OnInit} from '@angular/core';
import {MAT_SNACK_BAR_DATA, MatSnackBar, MatSnackBarConfig} from '@angular/material';

@Injectable()
export class HttpErrorInterceptor implements HttpInterceptor {
  constructor(private snackBar: MatSnackBar) {}
  configSuccess: MatSnackBarConfig = {
    duration: 3000,
    verticalPosition: 'top',
    panelClass: ['snackbar']
  };
  errorMessage: String;
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          if (!error.status) {
            console.log('error status is!!' + error.status);
            this.errorMessage = 'Please check your internet connection or try again later';
            const snackBarRef = this.snackBar.openFromComponent(ErrorSnackbarComponent, {
              data: this.errorMessage,
              ...this.configSuccess
            });
            snackBarRef.afterDismissed().subscribe(() => {
              this.errorMessage = '';
            });
          }
          return throwError(error);
        })
      );
  }
}

@Component({
  selector: 'app-error-snackbar',
  templateUrl: './error-snackbar.component.html',
})
export class ErrorSnackbarComponent implements OnInit {
  constructor( @Inject(MAT_SNACK_BAR_DATA) public data: any) { }
  ngOnInit() {}
}
