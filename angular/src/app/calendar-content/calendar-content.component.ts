import {Component} from '@angular/core';
@Component({
  selector: 'app-calendar-content',
  templateUrl: './calendar-content.component.html',
  styleUrls: ['./calendar-content.component.css']
})
export class CalendarContentComponent {
  constructor() {}
  cards: number[] = Array(35).fill(0);
}
