import {Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {RequestsService} from '../requests/requests.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-review-request-dialog',
  templateUrl: './resolve-pto-dialog.html',
  styleUrls: ['./resolve-pto-dialog.css']
})
export class ResolvePtoDialogComponent implements OnInit, OnDestroy {

  resolvePtoForm: FormGroup;
  approval: any = {};
  status: String;
  comment: String;
  resolvePtoSub: Subscription;
  isValid = true;

  ngOnInit() {}

  constructor(private fb: FormBuilder, private dialogRef: MatDialogRef<ResolvePtoDialogComponent>, private requestsService: RequestsService, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.resolvePtoForm = fb.group({
      'status': [null, Validators.compose([Validators.required, Validators.pattern('(approve|reject)')])],
      'comment': [null, Validators.compose([Validators.required, Validators.minLength(3)])]
    });
  }

  saveRequest(approval) {
    this.data.onSubmitSubject.next();
    this.approval.status = approval.status;
    this.approval.comment = approval.comment;
    this.resolvePtoSub = this.requestsService.resolvePto({
      id: this.data.id,
      status: this.approval.status,
      comment: this.approval.comment
    }).subscribe(updatedPto => {
      this.isValid = true;
      this.dialogRef.close(updatedPto);
    }, error => {
      this.resolvePtoForm.reset();
      this.isValid = false;
      this.data.onSubmitSubject.next();
    });
  }
  ngOnDestroy() {
    this.resolvePtoSub.unsubscribe();
  }

}
