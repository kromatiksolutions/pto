import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResolvePtoDialogComponent } from './resolve-pto-dialog.component';

describe('ResolvePtoDialogComponent', () => {
  let component: ResolvePtoDialogComponent;
  let fixture: ComponentFixture<ResolvePtoDialogComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResolvePtoDialogComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResolvePtoDialogComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
