import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import * as moment from 'moment';
import {RequestsService} from '../requests/requests.service';
import {CalendarService} from '../calendar/calendar.service';
import {Subscription} from 'rxjs';

const format = 'DD-MMM-YYYY';

@Component({
  selector: 'app-calendar-card',
  templateUrl: './calendar-card.component.html',
  styleUrls: ['./calendar-card.component.css']
})
export class CalendarCardComponent implements OnInit, OnDestroy {
  constructor(private calendarService: CalendarService, private requestsService: RequestsService) {
  }

  @Input() i: number;
  day: any;
  isWeekend: boolean;
  isOutOfSeason: boolean;
  changeDate: Subscription;
  setPtos: Subscription;
  ptos;
  activePtos;
  inRange = false;
  tmp: any;
  daySpliter: any;
  isDayOutOfSeason(day): boolean {
    return !(moment(day, format) >= this.requestsService.start && moment(day, format) <= this.requestsService.end);
  }
  isDayWeekend(day): boolean {
    return (moment(day, format).weekday() === 6 || moment(day, format).weekday() === 0);
  }


  ngOnInit() {
    this.calendarService.hoverRangeSubject.subscribe(hoveredDay => {
      if (this.calendarService.rangeCounter == 1) {
        if (this.dateCompare(hoveredDay, this.day, this.calendarService.startRange) === true) {
          this.inRange = true;
        } else if (moment(hoveredDay, format) >= moment(this.day, format) && moment(this.day, format) >= moment(this.calendarService.startRange, format)) {
          this.inRange = true;
        } else {
          this.inRange = false;
        }
      }
    });
    this.calendarService.resetRangeSubject.subscribe(() => {
      this.inRange = false;
      this.calendarService.rangeCounter = 0;
    });
    this.calendarService.changeRangeSubject.subscribe(() => {
      if (moment(this.calendarService.endRange, format) < moment(this.calendarService.startRange, format)) {
        this.tmp = this.calendarService.endRange;
        this.calendarService.endRange = this.calendarService.startRange;
        this.calendarService.startRange = this.tmp;
      }
      if (this.dateCompare(this.calendarService.startRange, this.day, this.calendarService.endRange) === true) {
        this.inRange = true;
      } else {
        this.inRange = false;
      }
      if (this.calendarService.rangeCounter == 0 && this.calendarService.equal == 1) {
        this.inRange = false;
      }
    });
    this.setPtos = this.requestsService.setPtos.subscribe((event: any) => {
      if (event.changeCalendarStartDate && event.failed !== true) {
        this.calendarService.firstDay = moment(this.requestsService.start);
        if (this.calendarService.firstDay.weekday() !== 1) {
          this.calendarService.firstDay = moment(this.requestsService.start).startOf('week').add(1, 'days');
        }
        if (this.calendarService.firstDay >= moment(this.requestsService.start).add(1, 'days')) {
          this.calendarService.firstDay.add(-7, 'days');
        }
        this.day = moment(this.calendarService.firstDay).add(this.i, 'days').format(format);
        this.daySpliter = this.day.split('-');
      }
      this.calendarService.selectedPto = null;
      this.activePtos = [];
      this.ptos = Object.assign([], event.ptos);
      this.ptos.map(pto => {
        if (moment(this.day, format).startOf('day') >= moment(pto.fromDate).startOf('day') && moment(this.day, format).startOf('day') <= moment(pto.toDate).startOf('day')) {
          this.activePtos.push({status: pto.status, selected: false});
        }
      });
      this.isWeekend = this.isDayWeekend(this.day);
      this.isOutOfSeason = this.isDayOutOfSeason(this.day);
    });
    this.activePtos = [];
    this.day = moment(this.calendarService.firstDay).add(this.i, 'days').format(format);
    this.daySpliter = this.day.split('-');
    this.isWeekend = this.isDayWeekend(this.day);
    if ((this.isOutOfSeason = this.isDayOutOfSeason(this.day))) {
      this.calendarService.changeCardNum(1, (this.i + 1));
    }
    this.changeDate = this.calendarService.changeDateSubject.subscribe(() => {
      this.day = moment(this.calendarService.firstDay).add(this.i, 'days').format(format);
      this.daySpliter = this.day.split('-');
      this.isWeekend = this.isDayWeekend(this.day);
      if ((this.isOutOfSeason = this.isDayOutOfSeason(this.day))) {
        this.calendarService.changeCardNum(1, (this.i + 1));
      } else {
        this.calendarService.changeCardNum(0, (this.i + 1));
      }
      this.activePtos = [];
      this.ptos.map(pto => {
        if (moment(this.day, format).startOf('day') >= moment(pto.fromDate).startOf('day') && moment(this.day, format).startOf('day') <= moment(pto.toDate).startOf('day')) {
          if (this.calendarService.selectedPto && this.calendarService.selectedPto._id === pto._id) {
            this.activePtos.push({status: pto.status, selected: true});
          } else {
            this.activePtos.push({status: pto.status, selected: false});
          }
        }
      });
    }); 
   }


  requestRange(date) {
    this.calendarService.rangePicker(date);
  }

  isHovered(date) {
    this.calendarService.rangeHovered(date);
  }

  dateCompare(day1, day2, day3) {
    if (moment(day1, format) <= moment(day2, format) && moment(day2, format) <= moment(day3, format)) {
      return true;
    } else {
      return false;
    }
  }

  ngOnDestroy(): void {
    this.changeDate.unsubscribe();
    this.setPtos.unsubscribe();
    this.calendarService.hoverRangeSubject.unsubscribe();
    this.calendarService.resetRangeSubject.unsubscribe();
    this.calendarService.changeRangeSubject.unsubscribe();
  }

}
