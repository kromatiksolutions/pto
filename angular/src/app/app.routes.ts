import { Routes } from "@angular/router";
import { AuthGuardService as AuthGuard } from "./auth/auth-guard.service";
import { AuthenticatedComponent } from "./auth/authenticated/authenticated.component";
import { AppComponent } from "./app.component";

export const ROUTES: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full', canActivate: [AuthGuard]},
  { path: 'authenticated', component: AuthenticatedComponent, pathMatch: 'full'},
  { path: 'home', component: AppComponent, canActivate: [AuthGuard]}
];
