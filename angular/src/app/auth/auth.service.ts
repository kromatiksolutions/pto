import { Injectable } from "@angular/core";
import { Router } from "@angular/router";
import { HttpClient } from "@angular/common/http";

@Injectable({
  providedIn: "root"
})
export class AuthService {

  public user;

  constructor(public router: Router, private http: HttpClient) {
  }

  public login(): void {
    window.location.href = '/login';
  }

  /**
   * This method job is to fetch currently logged in user
   * data for the frontend to use. 1) will check if there
   * is someone that is logged in, if yes then 2) user data
   * will be received and the UI will know to follow
   * permissions, if no then 3) it will cause redirect to
   * /login to authentication process
   */
  public auth(): void {
    this.http.get('/auth').subscribe(
      (activeUser => {
        this.user = activeUser;
        this.router.navigate(["/"]);
      }),
      (error => {
        console.log(error);
        this.login();
      })
    );
  }

  public logout(): void {
    this.user = undefined;
    window.location.href = '/logout';
  }

  public isAuthenticated(): boolean {
    return this.user !== undefined;
  }
}
