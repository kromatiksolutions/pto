import { Component, Inject } from "@angular/core";
import { FormControl, Validators } from "@angular/forms";
import { Pto } from "../requests/Pto";
import { RequestsService } from "../requests/requests.service";
import * as moment from "moment";
import { CalendarService } from "../calendar/calendar.service";
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: "dialog-component",
  templateUrl: "dialog.component.html",
  styleUrls: ["dialog.component.css"]
})
export class DialogComponent {
  ptos: Pto[] = [];
  pto: Pto = new Pto();
  emailHolder: string;
  emails: string[] = [];
  emailValidator = new FormControl("", [Validators.required, Validators.email]);
  emailPattern = "^[^@\\s]+@[^@\\s]+\\.[^@\\s]+$";
  valid = false;
  invalid = false;
  calledFromCalendar = false;
  startRange: any;
  endRange: any;


  constructor(
    private ptoService: RequestsService,
    private requestsService: RequestsService,
    private changeCalendarService: CalendarService,
    @Inject(MAT_DIALOG_DATA) public data: any){
    this.calledFromCalendar = data.openedRangeSelect
    if(this.calledFromCalendar == true){
      this.pto.fromDate = moment(this.changeCalendarService.startRange).format('M/D/YYYY');
      this.pto.toDate = moment(this.changeCalendarService.endRange).format('M/D/YYYY');
      this.startRange = moment(this.changeCalendarService.startRange).format('M/D/YYYY');
      this.endRange = moment(this.changeCalendarService.endRange).format('M/D/YYYY');
    }
  }

  ngOnInit() {
    this.pto.approvers = [];
  }

  getDate(date: string) {
    const momentDate = new Date(date);
    var formattedDate = moment(momentDate).format("YYYY-MM-DD");
    return formattedDate;
  }

  private delay(ms: number) {
    return new Promise(resolve => setTimeout(resolve, ms));
  }

  addEmail(email: string) {
    var br = 0;
    for (var i = 0; i < this.emails.length; i++) {
      if (email == this.emails[i]) {
        br++;
      } else {
      }
    }
    if (br === 0 && email.match(this.emailPattern)) {
      this.pto.approvers.push({ email: email });
      this.emails.push(email);
    } else {
      br = 0;
    }
  }

  deleteEmail(email: string) {
    for (var i = 0; i < this.emails.length; i++) {
      if (email === this.emails[i]) {
        this.emails.splice(i, 1);
      }
    }

    for (var i = 0; i < this.pto.approvers.length; i++) {
      if (email === this.pto.approvers[i]) {
        this.pto.approvers.splice(i, 1);
      }
    }
  }

  vacation() {
    this.pto.type = "Vacation";
  }

  sick() {
    this.pto.type = "Sick";
  }

  getErrorMessage() {
    return this.emailValidator.hasError("required")
      ? "Enter a valid email"
      : this.emailValidator.hasError("email")
      ? "Not a valid email"
      : "";
  }

  async saveRequest() {
    if (
      (this.pto.type === "Vacation" || this.pto.type === "Sick") &&
      (this.pto.approvers.length != 0 &&
        this.getDate(this.pto.fromDate) <= this.getDate(this.pto.toDate))
    ) {
      if(this.calledFromCalendar == false){
        this.pto.fromDate = moment(this.pto.fromDate).format('M/D/YYYY');
    }
      var d = this.pto.fromDate.split('/');
      var year = d[2];
      var month = d[0];
      this.pto.fromDate = this.getDate(this.pto.fromDate);
      this.pto.toDate = this.getDate(this.pto.toDate);
      this.ptoService.addPtos(this.pto).subscribe(
        async pto => {
          console.log("success", pto);
          if (month <= 6) {
            this.requestsService.changeSeason({
              changeValue: year - this.requestsService.currentSeason - 1,
              changeCalendarStartDate: true
            });
            this.changeCalendarService.jumpToPto(this.pto);
          } else {
            this.requestsService.changeSeason({
              changeValue: year - this.requestsService.currentSeason,
              changeCalendarStartDate: true
            });
            this.changeCalendarService.jumpToPto(this.pto);
            this.changeCalendarService.resetRangePicker();
          }
          this.valid = true;
          await this.delay(3000);
          this.valid = false;
        },
        err => console.log("error", err)
      );
    } else {
      this.invalid = true;
      await this.delay(3000);
      this.invalid = false;
    }
  }
}
