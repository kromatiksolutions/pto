import { Component, Inject } from '@angular/core';
import { AuthService } from '../auth/auth.service';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']

})

export class HeaderComponent {
  constructor(private authService: AuthService, public dialog: MatDialog, private translate: TranslateService) {
    translate.setDefaultLang('en');
  }
  
  logout() {
    this.authService.logout();
  }
  
  switchLanguage(language: string) {
    this.translate.use(language);
  }
}
