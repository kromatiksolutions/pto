import {Component, OnDestroy, OnInit, Output} from '@angular/core';
import {RequestsService} from '../requests.service';

import {Pto} from '../Pto';
import {Subscription} from 'rxjs';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-requests-list',
  templateUrl: './requests-list.component.html',
  styleUrls: ['./requests-list.component.css']
})
export class RequestsListComponent implements OnInit, OnDestroy {
  ptos: Pto[];
  loadRequests: boolean;
  @Output() loadFreeDaysCount: boolean;
  @Output() freeDaysCount;
  changeSeason: Subscription;
  getRequests: Subscription;
  getFreeDaysCount: Subscription;

  constructor(private requestsService: RequestsService, public dialog: MatDialog) {

  }

  ngOnInit() {
    this.loadFreeDaysCount = true;
    this.getFreeDaysCount = this.requestsService.getRemainingFreeDays().subscribe((event: any) => {
      this.freeDaysCount = event.freeDaysCount;
      this.loadFreeDaysCount = false;
    }, (error) => {
      this.loadFreeDaysCount = false;
      this.freeDaysCount = '-';
    });
    this.loadRequests = true;
    this.getRequests = this.requestsService.getRequests().subscribe((ptos) => {
      this.requestsService.ptos = Object.assign([], ptos);
      this.ptos = Object.assign([], ptos);
      this.loadRequests = false;
      this.requestsService.setPtos.next({
        ptos: this.ptos,
        changeCalendarStartDate: false,
        currentSeason: this.requestsService.currentSeason,
        error: false,
        changeValue: 0
      });
    }, error => {
      this.loadRequests = false;
      this.requestsService.ptos = [];
      this.ptos = [];
      this.requestsService.setPtos.next({
        ptos: this.ptos,
        changeCalendarStartDate: false,
        currentSeason: this.requestsService.currentSeason,
        failed: true,
        changeValue: 0
      });
    });
    this.changeSeason = this.requestsService.changeSeasonSubject.subscribe(event => {
      this.loadRequests = true;
      this.getRequests = this.requestsService.getRequests(event.currentSeason).subscribe(ptos => {
        this.requestsService.ptos = Object.assign([], ptos);
        this.ptos = Object.assign([], ptos);
        this.loadRequests = false;
        this.requestsService.setPtos.next({
          ptos: this.ptos,
          changeCalendarStartDate: event.changeCalendarStartDate,
          currentSeason: event.currentSeason,
          failed: false,
          changeValue: event.changeValue
        });
      }, (error) => {
        this.loadRequests = false;
        this.requestsService.ptos = [];
        this.ptos = [];
        this.requestsService.setPtos.next({
          ptos: this.ptos,
          changeCalendarStartDate: event.changeCalendarStartDate,
          currentSeason: event.currentSeason,
          failed: true,
          changeValue: event.changeValue
        });
      });
      if (this.requestsService.listMinePtos) {
        this.loadFreeDaysCount = true;
        this.getFreeDaysCount = this.requestsService.getRemainingFreeDays().subscribe((freeDaysEvent: any) => {
          this.freeDaysCount = freeDaysEvent.freeDaysCount;
          this.loadFreeDaysCount = false;
        }, (error) => {
          this.loadFreeDaysCount = false;
          this.freeDaysCount = '-';
        });
      }
    });
  }
  changeRoute(val) {
    if (val) {
      this.requestsService.listMinePtos = false;
    } else {
      this.requestsService.listMinePtos = true;
    }
    this.requestsService.changeSeasonSubject.next({
      currentSeason: this.requestsService.currentSeason,
      changeCalendarStartDate: false,
      changeValue: 0,
    });
  }
  ngOnDestroy(): void {
    this.getRequests.unsubscribe();
    this.changeSeason.unsubscribe();
    this.getFreeDaysCount.unsubscribe();
  }
}
