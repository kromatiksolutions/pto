import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import * as moment from 'moment';
import {faPen, faAngleRight} from '@fortawesome/free-solid-svg-icons';
import {CalendarService} from '../../calendar/calendar.service';
import {RequestsService} from '../requests.service';
import {MatDialog} from '@angular/material';
import {Pto} from '../Pto';
import {Subject, Subscription} from 'rxjs';

import {ResolvePtoDialogComponent} from '../../resolve-pto-dialog/resolve-pto-dialog.component';

@Component({
  selector: 'app-request-item',
  templateUrl: './request-item.component.html',
  styleUrls: ['./request-item.component.css']
})
export class RequestItemComponent implements OnInit, OnDestroy {
  @Input() pto: any;
  workDaysCount: number;
  faPen = faPen;
  faAngleRight = faAngleRight;
  listMinePtos = this.requestsService.listMinePtos;
  changeSeason: Subscription;
  reviewPtoSub: Subscription;
  openDialogSub: Subscription;
  closeDialogSub: Subscription;
  onSubmitSubscription: Subscription;
  onSubmitSubject: Subject<any> = new Subject();
  loading = false;
  dialogRef: any;
  fromDateSpliter: any;
  toDateSpliter: any;
  constructor(private calendarService: CalendarService, private requestsService: RequestsService, public dialog: MatDialog) {}

  jumpToPto() {
    this.calendarService.jumpToPto(this.pto);
  }

  resolvePto() {
    this.dialogRef = this.dialog.open(ResolvePtoDialogComponent, {
      width: '500px',
      panelClass: 'custom-dialog-container',
      data: {
        id: this.pto._id,
        onSubmitSubject: this.onSubmitSubject
      }
    });
    this.closeDialogSub = this.dialogRef.afterClosed().subscribe((pto): Pto => {
      if (!pto._id) {
        return;
      }
      this.pto.status = pto.status;
      this.pto.comment = pto.comment;
      this.loading = false;
      this.requestsService.setPtos.next({
        ptos: this.requestsService.ptos,
        changeCalendarStartDate: false,
        currentSeason: this.requestsService.currentSeason,
        error: false,
        changeValue: 0
      });
    });
  }
  ngOnInit() {
    this.pto.fromDate = moment(this.pto.fromDate).format('DD-MMM-YYYY');
    this.fromDateSpliter = this.pto.fromDate.split('-')
    this.pto.toDate = moment(this.pto.toDate).format('DD-MMM-YYYY');
    this.toDateSpliter = this.pto.toDate.split('-')
    this.workDaysCount = this.pto.workDaysCount;

    this.changeSeason = this.requestsService.changeSeasonSubject.subscribe(() => {
      this.listMinePtos = this.requestsService.listMinePtos;
    });
    this.onSubmitSubscription = this.onSubmitSubject.subscribe(() => {
      this.loading = !this.loading;
    });
  }


  ngOnDestroy() {
    this.changeSeason.unsubscribe();
    if (this.reviewPtoSub) {
      this.reviewPtoSub.unsubscribe();
    }
    if (this.openDialogSub) {
      this.openDialogSub.unsubscribe();
    }
    if (this.onSubmitSubscription) {
      this.onSubmitSubscription.unsubscribe();
    }
    if (this.closeDialogSub) {
      this.closeDialogSub.unsubscribe();
    }
  }
}

