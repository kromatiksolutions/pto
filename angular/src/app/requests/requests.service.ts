import {Injectable, OnDestroy} from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable, Subject, Subscription} from 'rxjs';

import { environment } from '../../environments/environment';
import * as moment from 'moment';
import { Moment } from 'moment';
import { Pto } from './Pto';

const format = 'DD-MM-YYYY';

@Injectable({
  providedIn: 'root'
})
export class RequestsService implements OnDestroy {
  setPtosSub: Subscription;
  currentDate: Moment = moment();
  currentSeason: number = this.currentDate.year();
  start: Moment;
  end: Moment;
  ptos: any;
  listMinePtos = true;
  freeDaysCount: number;

  public changeSeasonSubject: Subject<any> = new Subject<any>();
  public setPtos: Subject<any> = new Subject<any>();

  constructor(private http: HttpClient) {
    this.start = moment('01-07-' + this.currentSeason, format);
    this.end = moment('30-06-' + (this.currentSeason + 1), format);
    if (this.currentDate <= this.start) {
      this.changeSeason({changeValue: -1, changeCalendarStartDate: false});
    } else if (this.currentDate >= this.end) {
      this.changeSeason({changeValue: 1, changeCalendarStartDate: false});
    }
    this.setPtosSub = this.setPtos.subscribe(event => {
      if (event.failed === true) {
        this.currentSeason = event.currentSeason - event.changeValue;
      }
      this.start = moment('01-07-' + this.currentSeason, format);
      this.end = moment('30-06-' + (this.currentSeason + 1), format);
    });
  }

  getRequests(year: number = this.currentSeason): Observable<Object> {
    if (this.listMinePtos) {
      return this.http.get(`${environment.apiUrl}/api/v1/ptos?year=${year}`);
    }
    return this.http.get(`${environment.apiUrl}/api/v1/approvals?year=${year}`);
  }

  changeSeason(changeEvent: any): void {
    this.currentSeason += changeEvent.changeValue;
    this.start = moment('01-07-' + this.currentSeason, format);
    this.end = moment('30-06-' + (this.currentSeason + 1), format);
    this.changeSeasonSubject.next({
      currentSeason: this.currentSeason,
      changeValue: changeEvent.changeValue,
      changeCalendarStartDate: changeEvent.changeCalendarStartDate
    });

  }

  getRemainingFreeDays(): Observable<Object> {
    return this.http.get(`${environment.apiUrl}/api/v1/ptos/remaining?year=${this.currentSeason}`);
  }

  resolvePto(approval): Observable<Object> {
    return this.http.post(`${environment.apiUrl}/api/v1/approvals/${approval.id}/${approval.status}`, {
      comment: approval.comment
    });
  }

  addPtos(newPto: Pto) {
    console.log(newPto);
    return this.http.post(`${environment.apiUrl}/api/v1/ptos`, newPto);
  }
  ngOnDestroy(): void {
    this.setPtosSub.unsubscribe();
  }
}
