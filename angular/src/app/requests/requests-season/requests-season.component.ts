import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {faAngleLeft, faAngleRight} from '@fortawesome/free-solid-svg-icons';
import {RequestsService} from '../requests.service';
import {DialogComponent} from '../../header/dialog.component';
import {MatDialog} from '@angular/material';
import {Subscription} from 'rxjs';
import { CalendarService } from '../../calendar/calendar.service';

@Component({
  selector: 'app-requests-season',
  templateUrl: './requests-season.component.html',
  styleUrls: ['./requests-season.component.css']
})
export class RequestsSeasonComponent implements OnInit, OnDestroy {
  faLeft = faAngleLeft;
  faRight = faAngleRight;
  @Input() loadFreeDaysCount;
  @Input() freeDaysCount;
  @Input() loadRequests;
  currentSeason = this.requestsService.currentSeason;
  changeSeason: Subscription;

  constructor(private requestsService: RequestsService, public dialog: MatDialog, private calendarService: CalendarService) {}
  ngOnInit() {
    this.changeSeason = this.requestsService.setPtos.subscribe(event => {
      if (event.failed === true) {
        this.currentSeason = NaN;
        return;
      }
      this.currentSeason = event.currentSeason;
    });
  }

  onChangeSeason(value): void {
    this.requestsService.changeSeason({changeValue: value, changeCalendarStartDate: true});
    this.calendarService.resetRangePicker();
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(DialogComponent, {
      width: '650px',
      panelClass: 'custom-dialog-container',
      data: {openedRangeSelect: false},
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log('The dialog was closed');
      this.calendarService.resetRangePicker();
    });
  }
 
  ngOnDestroy(): void { 
    this.changeSeason.unsubscribe();
  }
}
