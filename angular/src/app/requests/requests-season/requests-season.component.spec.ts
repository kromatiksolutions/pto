import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RequestsSeasonComponent } from './requests-season.component';

describe('RequestsSeasonComponent', () => {
  let component: RequestsSeasonComponent;
  let fixture: ComponentFixture<RequestsSeasonComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RequestsSeasonComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RequestsSeasonComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
