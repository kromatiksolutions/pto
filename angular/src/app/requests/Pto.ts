export class Pto {
  _id: string;
  approvers: Object[];
  fromDate: any;
  toDate: any;
  type: string;
  comment: string;
  status: string;
  userId: string;
}
