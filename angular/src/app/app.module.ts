import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import {MatFormFieldModule} from '@angular/material/form-field';
import { MatToolbarModule, MatDialogModule, MatButtonToggleModule, MatNativeDateModule } from '@angular/material';
import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import { AuthService } from './auth/auth.service';
import { UnAuthInterceptor } from './auth/unauth.interceptor';
import { HTTP_INTERCEPTORS, HttpClientModule, HttpClient } from '@angular/common/http';
import { AuthenticatedComponent } from './auth/authenticated/authenticated.component';
import { RouterModule } from '@angular/router';
import { ROUTES } from './app.routes';
import { MatDatepickerModule} from '@angular/material/datepicker';
import { DialogComponent } from './header/dialog.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DemoMaterialModule } from './material-module';
import { RequestsListComponent } from './requests/requests-list/requests-list.component';
import { RequestItemComponent } from './requests/request-item/request-item.component';
import {CalendarComponent} from './calendar/calendar.component';
import {MatSidenavModule} from '@angular/material/sidenav';
import { RequestsSeasonComponent } from './requests/requests-season/requests-season.component';
import { CalendarNavComponent } from './calendar-nav/calendar-nav.component';
import { CalendarContentComponent } from './calendar-content/calendar-content.component';
import { CalendarCardComponent } from './calendar-card/calendar-card.component';
import { ResolvePtoDialogComponent } from './resolve-pto-dialog/resolve-pto-dialog.component';
import {HttpErrorInterceptor, ErrorSnackbarComponent} from './http-error/http-error.interceptor';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, 'assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthenticatedComponent,
    DialogComponent,
    RequestsListComponent,
    RequestItemComponent,
    RequestsSeasonComponent,
    CalendarComponent,
    CalendarNavComponent,
    CalendarContentComponent,
    CalendarCardComponent,
    ResolvePtoDialogComponent,
    ErrorSnackbarComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FontAwesomeModule,
    MatToolbarModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    RouterModule.forRoot(ROUTES, { onSameUrlNavigation: 'reload' }),
    MatDatepickerModule,
    MatFormFieldModule,
    MatDialogModule,
    MatButtonToggleModule,
    MatNativeDateModule,
    FormsModule,
    ReactiveFormsModule,
    DemoMaterialModule,
    MatSidenavModule,
    TranslateModule.forRoot({
      loader: {
      provide: TranslateLoader,
      useFactory: (createTranslateLoader),
      deps: [HttpClient]
      }
    })
  ],
  entryComponents: [
    DialogComponent,
    ResolvePtoDialogComponent,
    ErrorSnackbarComponent
  ],
  providers: [
    AuthService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: UnAuthInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true,
    }
    ],
  bootstrap: [AppComponent]
})
export class AppModule { }
