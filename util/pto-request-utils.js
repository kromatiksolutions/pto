const { isEmpty } = require('lodash');
const util = require('util');
const log4js = require('log4js');
const { NotFoundError } = require('../errors/errors');

const logger = log4js.getLogger();

const checkIfExists = (pto, id, callerId) => {
  if (isEmpty(pto)) {
    const message = util.format(
      'action=FindPtoRequest, id=%s, callerId=%s, Pto request not found',
      id,
      callerId
    );
    logger.error(message);
    throw new NotFoundError(message);
  }
};

module.exports = checkIfExists;
