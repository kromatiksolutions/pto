const log4js = require('log4js');
const util = require('util');
const PtoRequest = require('../mongo/model/pto-request');
const checkIfExists = require('../util/pto-request-utils');

const logger = log4js.getLogger();

const initConditions = (id, callerId) => {
  return {
    $and: [{ _id: id }, { userId: callerId }],
  };
};

const getOverlapCount = async (ptoRequest, callerId) => {
  return PtoRequest.find({
    userId: callerId,
    $or: [
      {
        fromDate: { $gte: new Date(ptoRequest.fromDate) },
        toDate: { $lte: new Date(ptoRequest.toDate) },
      },
      {
        fromDate: { $lte: new Date(ptoRequest.toDate) },
        toDate: { $gte: new Date(ptoRequest.fromDate) },
      },
    ],
  }).countDocuments();
};

const getAllPtoRequests = async (match, options, year) => {
  try {
    return await PtoRequest.findBySeason(match, options, year);
  } catch (e) {
    throw new Error(e);
  }
};

const getPtoRequest = async (id, callerId) => {
  const conditions = initConditions(id, callerId);

  const pto = await PtoRequest.find(conditions);

  checkIfExists(pto, id, callerId);

  return pto;
};

const updatePtoRequest = async (id, ptoRequest, callerId) => {
  const conditions = initConditions(id, callerId);

  const pto = await PtoRequest.findOneAndUpdate(
    conditions,
    { $set: ptoRequest },
    { new: true }
  );

  checkIfExists(pto, id, callerId);

  return pto;
};

const deletePtoRequest = async (id, callerId) => {
  const conditions = initConditions(id, callerId);

  const pto = await PtoRequest.findOneAndDelete(conditions);

  checkIfExists(pto, id, callerId);

  return pto;
};

const savePtoRequest = async (ptoRequest, callerId, emailAddress) => {
  const overlapCount = await getOverlapCount(ptoRequest, callerId);

  if (overlapCount > 0) {
    const message = util.format(
      'action=SavePtoRequestFailed, callerId=%s, Overlapping PTO requests, please check your date-range',
      callerId
    );
    logger.error(message);
    throw new Error(message);
  }

  const newPtoRequest = new PtoRequest({
    userId: callerId,
    userEmail: emailAddress,
    fromDate: ptoRequest.fromDate,
    toDate: ptoRequest.toDate,
    approvers: ptoRequest.approvers,
    type: ptoRequest.type,
    status: 'pending',
    comment: '',
  });

  await newPtoRequest.save();

  return newPtoRequest;
};

module.exports = {
  getAllPtoRequests,
  savePtoRequest,
  getPtoRequest,
  updatePtoRequest,
  deletePtoRequest,
};
