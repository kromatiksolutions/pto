const jsLogger = require('log4js');
const util = require('util');
const User = require('../mongo/model/user');

const logger = jsLogger.getLogger();

const buildUser = async profile => {
  return new User({
    googleId: profile.id,
    userName: profile.displayName,
    emailAddress: profile._json.email,
  });
};

const findUserByGoogleId = async id => {
  try {
    logger.debug('action=FindUserByGoogleId, id=%s, Fetching user by id', id);

    return User.findOne({ googleId: id });
  } catch (e) {
    const message = util.format(
      'action=FindUserByGoogleIdFailed, id=%s, error=%s, Error ocurred while trying to fetch user',
      id,
      e
    );
    logger.debug(message);
    throw new Error(message);
  }
};

const createUser = async profile => {
  try {
    logger.debug('action=CreateUser, googleId=%s, Creating user', profile.id);
    const profileToSave = await buildUser(profile);

    return await profileToSave.save();
  } catch (e) {
    throw new Error(e);
  }
};

module.exports = {
  createUser,
  findUserByGoogleId,
};
