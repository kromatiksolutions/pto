const PtoRequest = require('../mongo/model/pto-request');
const { NotFoundError } = require('../errors/errors');

const updateOptions = {
  new: true, // to return the updated document
};

const getAllApprovals = async (match, options, year) => {
  return PtoRequest.findBySeason(match, options, year);
};

const getApprovalRequest = async (id, approver) => {
  const pto = await PtoRequest.findOne({
    _id: id,
    'approvers.email': approver,
  });

  if (!pto) {
    throw new NotFoundError('PTO not found');
  }

  return pto;
};

const approvePtoRequest = async (approvalRequest, id, approver) => {
  const pto = await PtoRequest.findOneAndUpdate(
    { _id: id, 'approvers.email': approver },
    { $set: { status: 'approved', comment: approvalRequest.comment } },
    updateOptions
  );

  if (!pto) {
    throw new NotFoundError('PTO not found');
  }

  return pto;
};

const rejectPtoRequest = async (approvalRequest, id, approver) => {
  const pto = await PtoRequest.findOneAndUpdate(
    { _id: id, 'approvers.email': approver },
    { $set: { status: 'rejected', comment: approvalRequest.comment } },
    updateOptions
  );

  if (!pto) {
    throw new NotFoundError('PTO not found');
  }

  return pto;
};

module.exports = {
  getAllApprovals,
  getApprovalRequest,
  approvePtoRequest,
  rejectPtoRequest,
};
