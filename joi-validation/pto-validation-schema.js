const Joi = require('joi');

const ptoRequestSchema = Joi.object().keys({
  fromDate: Joi.date().required(),
  toDate: Joi.date().required(),
  type: Joi.string().required(),
  approvers: Joi.array().items(
    Joi.object()
      .keys({
        email: Joi.string()
          .email()
          .required(),
      })
      .required()
  ),
});

module.exports = ptoRequestSchema;
