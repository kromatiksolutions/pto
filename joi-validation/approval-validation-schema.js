const Joi = require('joi');

const approvalRequestSchema = Joi.object().keys({
  comment: Joi.string().required(),
  status: Joi.string()
    .valid(['pending', 'approved', 'rejected'])
    .default('pending'),
});

module.exports = approvalRequestSchema;
