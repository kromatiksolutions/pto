const Joi = require('joi');

const userSchema = Joi.object().keys({
  id: Joi.string().required(),
  displayName: Joi.string()
    .min(4)
    .max(100)
    .required(),
  email: Joi.string()
    .email({ minDomainAtoms: 2 })
    .required(),
});

module.exports = { userSchema };
