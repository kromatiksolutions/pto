require('@babel/polyfill');
const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
    entry: ['@babel/polyfill', './src/main.js'],
    output: {
        path: path.resolve(__dirname, '../public'),
        filename: 'bundle.js',
    },
    module: {
        rules: [{
            loader: 'babel-loader',
            test: /\.js$/,
            exclude: /node_modules/
        }, {
            test: /\.s?css$/,
            use: [
                'style-loader',
                'css-loader',
                'sass-loader'
            ]
        }, {
            test: /\.html$/,
            use: ['html-loader']
        }]
    },
    plugins: [
      new HtmlWebpackPlugin({
          template: 'src/index.html'
      })
    ],
    devtool: 'cheap-module-eval-source-map',
    devServer: {
        contentBase: path.join(__dirname, '../public'),
        port: 8080,
        proxy: {
            '/': {
                target: 'http://localhost:3000/',
                secure: false
            }
        }
    }
};