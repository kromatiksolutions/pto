import * as Filter from '../actions/filter-actions';

const initialState = {
  mine: false,
  approvals: false,
  season: {}
};

function filterReducer(state = initialState, action) {
  switch (action.type) {
    case Filter.ALL:
    case Filter.MINE:
    case Filter.APPROVALS:
      return Object.assign({}, state, action.payload);
    case Filter.CURRENT_SEASON:
      return Object.assign({}, state, {
        season: action.payload
      });
    case Filter.INCREMENT_SEASON:
      return Object.assign({}, state, {
        season: {
          start: state.season.start + 1,
          end: state.season.end + 1
        }
      });
    case Filter.DECREMENT_SEASON:
      return Object.assign({}, state, {
        season: {
          start: state.season.start - 1,
          end: state.season.end - 1
        }
      });
    default:
      return state;
  }
}

export default filterReducer;