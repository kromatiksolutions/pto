import * as PTO from '../actions/pto-actions';

const initialState = [];

function ptoReducer(state = initialState, action) {
  switch (action.type) {
    case PTO.GET_PTOS:
      return [...action.payload];
    case PTO.ADD_PTO:
      return [...state, action.payload]; // TODO da refreshuva ptos so ist id
    case PTO.REMOVE_PTO:
      return state.filter(pto => pto.id !== action.payload.id);
    default:
      return state;
  }
}

export default ptoReducer;