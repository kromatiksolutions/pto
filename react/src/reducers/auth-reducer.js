import * as Auth from '../actions/auth-actions';

const initialState = {
  user: undefined,
  error: undefined
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    case Auth.AUTHENTICATE:
      return Object.assign({}, state, {
        user: action.payload.user
        });
    case Auth.UNAUTHENTICATE:
      return Object.assign({}, state, {
        user: undefined
        });
    case Auth.AUTHENTICATIONERROR:
      return Object.assign({}, state, {
        error: action.payload.error
        });
    default:
      return state;
  }
}

export default authReducer;