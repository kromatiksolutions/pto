import {combineReducers} from 'redux';
import {connectRouter} from 'connected-react-router';
import authReducer from './auth-reducer';
import ptoReducer from './pto-reducer';
import filterReducer from './filter-reducer';

const rootReducer = history => combineReducers({
  router: connectRouter(history),
  auth: authReducer,
  ptos: ptoReducer,
  filter: filterReducer
});

export default rootReducer;