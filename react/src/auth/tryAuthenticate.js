import axios from 'axios';
import {store} from '../main';
import {authenticate, authenticationError} from '../actions/auth-actions';

export default function tryAuthenticate() {
  axios.get('/auth')
    .then(response => {
      store.dispatch(authenticate(response.data));
    })
    .catch(error => {
      store.dispatch(authenticationError(error));
    })
}