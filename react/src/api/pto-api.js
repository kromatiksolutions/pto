import axios from 'axios';
import {store} from '../main';
import {addPTO, getPTOs, removePTO} from '../actions/pto-actions';

export function createPTO(params) {
  console.log(params);
  axios.post('/api/v1/ptos', params)
    .then(response => {
      console.log(response);
      store.dispatch(addPTO(response.data));
    })
    .catch(error => console.log(error));
}

export function getAllPTO() {
  axios.get('/api/v1/ptos')
    .then(response => {
      console.log(response);
      store.dispatch(getPTOs(response.data));
    })
    .catch(response => console.log(response));
}

export function getPTOForSeason(year) {
  axios.get(`/api/v1/ptos?year=${year}`)
    .then(response => {
      console.log(response);
      store.dispatch(getPTOs(response.data));
    })
    .catch(response => console.log(response));
}

export function deletePTO(id) {
  axios.delete(`/api/v1/ptos/${id}`)
    .then(response => {
      console.log(response);
      store.dispatch(removePTO(id));
    })
    .catch(response => console.log(response));
}