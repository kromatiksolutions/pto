import {applyMiddleware, compose, createStore} from 'redux';
import {routerMiddleware} from 'connected-react-router';
import {createBrowserHistory} from 'history';
import rootReducer from './reducers/root-reducer';
import tryAuthenticate from './auth/tryAuthenticate';

export const history = createBrowserHistory();

export default function configureStore(preloadedState) {
  return createStore(
    rootReducer(history), // root reducer with router state
    preloadedState,
    compose(
      applyMiddleware(
        routerMiddleware(history),
        loggerMiddleware,
        authMiddleware
      ),
    ),
  );
};

// MIDDLEWARE:

const loggerMiddleware = store => next => action => {
  console.group(action.type);
  console.info('dispatching', action);
  let result = next(action);
  console.log('next state', store.getState());
  console.groupEnd();
  return result;
};

const authMiddleware = store => next => action => {
  if (hasAuthenticatedUser(store.getState()) || !isAPIAction(action)) {
    return next(action);
  } else {
    tryAuthenticate(store);
  }
};

function hasAuthenticatedUser(state) {
  return state.auth && state.auth.user;
}

function isAPIAction(action) {
  return action.meta && action.meta.type === 'API';
}