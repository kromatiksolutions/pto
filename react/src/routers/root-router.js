import React from 'react';
import {Link, Route, Switch} from 'react-router-dom';
import {ConnectedRouter} from 'connected-react-router';
import Home from '../components/Home';
import Header from '../components/header/Header';
import Auth from '../components/Auth';
import Account from '../components/Account';

const NotFoundPage = () => (
  <p>404 - <Link to="/">Home</Link></p>
);

// const RootRouter = (props) => (
// //   <ConnectedRouter history={props.history}>
// //     <div>
// //       <Header/>
// //       <Switch>
// //         <Route path="/" component={Home} exact={true} />
// //         <Route path="/account" component={Account} />
// //         <Route path="/authenticated" component={Auth}/>
// //         <Route component={NotFoundPage} />
// //       </Switch>
// //     </div>
// //   </ConnectedRouter>
// // );

const RootRouter = (props) => (
  <ConnectedRouter history={props.history}>
    {/*<div>*/}
      <Header/>
      {/*<div>*/}
        <Switch>
          <Route path="/" component={Home} exact={true} />
          <Route path="/account" component={Account} />
          <Route path="/authenticated" component={Auth}/>
          <Route component={NotFoundPage} />
        </Switch>
      {/*</div>*/}
    {/*</div>*/}
  </ConnectedRouter>
);

export default RootRouter;