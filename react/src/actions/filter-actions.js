// ACTION TYPES:

export const ALL = 'ALL';
export const MINE = 'MINE';
export const APPROVALS = 'APPROVALS';
export const CURRENT_SEASON = 'CURRENT_SEASON';
export const INCREMENT_SEASON = 'INCREMENT_SEASON';
export const DECREMENT_SEASON = 'DECREMENT_SEASON';

// ACTION CREATORS:

export function filter(filter) {
    switch(filter) {
      case ALL:
        return {
          type: ALL,
          payload: {
            mine: false,
            approvals: false
        }};
      case MINE:
        return {
          type: MINE,
          payload: {
            mine: true,
            approvals: false
        }};
      case APPROVALS:
        return {
          type: APPROVALS,
          payload: {
            mine: false,
            approvals: true
        }};
  }
}

export function currentSeason(season) {
  return {
    type: CURRENT_SEASON,
    payload: season
  }
}

export function incrementSeason() {
  return {
    type: INCREMENT_SEASON
  }
}

export function decrementSeason() {
  return {
    type: DECREMENT_SEASON
  }
}
