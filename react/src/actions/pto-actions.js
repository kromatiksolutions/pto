// ACTION TYPES:

export const GET_PTOS = 'GET_PTOS';
export const GET_PTO = 'GET_PTO';
export const ADD_PTO = 'ADD_PTO';
export const REMOVE_PTO = 'REMOVE_PTO';

// ACTION CREATORS:

export function getPTOs(ptosData) {
  return {
    type: GET_PTOS,
    payload: ptosData.map(data => {
      return {
        id: data._id,
        fromDate: data.fromDate.split('T')[0],
        toDate: data.toDate.split('T')[0],
        type: data.type,
        approvers: data.approvers.map(approver => approver.email),
        status: data.status,
        comment: data.comment
      }
    })
  }
}

export function addPTO(data) {
  return {
    type: ADD_PTO,
    payload: {
      id: data._id,
      fromDate: data.fromDate.split('T')[0],
      toDate: data.toDate.split('T')[0],
      type: data.type,
      approvers: data.approvers.map(approver => approver.email),
      status: data.status,
      comment: data.comment
    }
  }
}

export function removePTO(id) {
  return {
    type: REMOVE_PTO,
    payload: {
      id: id
    }
  }
}