// ACTION TYPES:

export const AUTHENTICATE = 'AUTHENTICATE';
export const UNAUTHENTICATE = 'UNAUTHENTICATE';
export const AUTHENTICATIONERROR = 'AUTHENTICATIONERROR';

// ACTION CREATORS:

export function authenticate(user) {
  return {
    type: AUTHENTICATE,
    payload: {
      user: user
    }
  }
}

export function unAuthenticate() {
  return {
    type: UNAUTHENTICATE,
    payload: {
      user: undefined
    }
  }
}

export function authenticationError(error) {
  return {
    type: AUTHENTICATIONERROR,
    payload: {
      error: error
    }
  }
}