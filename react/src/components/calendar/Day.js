import React from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowUp from '@material-ui/icons/KeyboardArrowUp';
import KeyboardArrowDown from '@material-ui/icons/KeyboardArrowDown';

export default class Day extends React.Component {

  styles = {
    root: {
      padding: 10,
      margin: 10,
      background: 'green',
      width: 120,
      height: 100,
      alignItems: 'center'
    }
  };

  constructor(props) {
    super(props);
  }

  render() {
    const { root } = this.styles;
    return (
      <Grid item style={root}>
        <p>12.MAY.2018</p>
        <span>a</span>
      </Grid>
    )
  }
}
