import React, {Fragment} from "react";
import {connect} from "react-redux";
import {push} from 'connected-react-router';
import Popper from '@material-ui/core/Popper';
import Fade from '@material-ui/core/Fade';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import Avatar from '@material-ui/core/Avatar';
import MenuList from '@material-ui/core/MenuList'
import MenuItem from '@material-ui/core/MenuItem'
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import {store} from '../../main';
import Divider from '@material-ui/core/Divider';

class AccountPopper extends React.Component {

  styles = {
    avatar: {
      margin: 10,
      width: 72,
      height: 72
    },
    text: {
      marginLeft: 15,
      marginRight: 15
    }
  };

  constructor(props) {
    super(props);
  }

  handleAccountClick = event => {
    this.props.onHandleMenuClick(event);
    store.dispatch(push('/account'));
  };

  handleLoginClick = event => {
    this.props.onHandleMenuClick(event);
    window.location.href = '/login';
  };

  handleLogoutClick = event => {
    this.props.onHandleMenuClick(event);
    window.location.href = '/logout';
  };

  handleClickAway = () => {
    this.props.onHandleClickAway();
  };

  render() {
    const { user, open, anchorEl } = this.props;
    const { avatar, text } = this.styles;

    return (
      <Popper open={open} anchorEl={anchorEl} transition>
        {({ TransitionProps }) => (
          <Fade {...TransitionProps} timeout={50}>
            <ClickAwayListener onClickAway={this.handleClickAway}>
              <Paper>
                <Grid container spacing={16} direction='column' alignContent='stretch' wrap='wrap'>
                  <Grid container spacing={8} direction='column' alignItems='center'>
                    {user
                      ?
                        <Fragment>
                        <Grid item>
                          <Avatar src={user.picture} style={avatar}/>
                        </Grid>
                        <Grid item>
                          <Typography variant='body1' style={text}>{user.displayName}</Typography>
                        </Grid>
                        </Fragment>
                      : null
                    }
                  </Grid>
                  <Grid item>
                    <MenuList>
                      {user
                        ?
                          <div>
                            <Divider />
                            <MenuItem onClick={this.handleAccountClick}>Account</MenuItem>
                            <MenuItem onClick={this.handleLogoutClick}>Logout</MenuItem>
                          </div>
                        : <MenuItem onClick={this.handleLoginClick}>Login</MenuItem>
                      }
                    </MenuList>
                  </Grid>
                </Grid>
              </Paper>
            </ClickAwayListener>
          </Fade>
        )}
      </Popper>
    )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
};

export default connect(mapStateToProps)(AccountPopper);