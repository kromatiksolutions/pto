import React from 'react';
import {connect} from 'react-redux';
import {push} from 'connected-react-router';
import {store} from '../../main';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar'
import IconButton from '@material-ui/core/IconButton';
import Avatar from '@material-ui/core/Avatar';
import AccountCircle from '@material-ui/icons/AccountCircle';
import AccountPopper from './AccountPopper';
import Home from '@material-ui/icons/Home';

class Header extends React.Component {

  state = {
    anchorEl: null,
    open: false
  };

  styles = {
    grow: {
      flexGrow: 1
    },
    avatar: {
      margin: 10,
      width: 32,
      height: 32
    }
  };

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      anchorEl: null
    };
  }

  handleHomeClick = () => {
    store.dispatch(push('/'));
  };

  handleMenuClick = event => {
    this.setState({
      anchorEl: event.currentTarget,
      open: !this.state.open
  })};

  handleClickAway = () => {
    this.setState({
      anchorEl: undefined,
      open: false
    });
  };

  // render() {
  //   const { anchorEl, open } = this.state;
  //   const { user } = this.props;
  //   const { grow, avatar } = this.styles;
  //
  //   return (
  //     <AppBar position='static' style={grow}>
  //       <Toolbar>
  //         <IconButton onClick={this.handleHomeClick}>
  //           <Home />
  //         </IconButton>
  //         <div style={grow}/>
  //         <IconButton onClick={this.handleMenuClick}>
  //           {user
  //             ? <Avatar alt='User' src={user.picture} style={avatar}/>
  //             : <AccountCircle style={avatar}/>
  //           }
  //         </IconButton>
  //         <AccountPopper
  //           open={open}
  //           anchorEl={anchorEl}
  //           onHandleMenuClick={this.handleMenuClick}
  //           onHandleClickAway={this.handleClickAway}
  //           transition/>
  //       </Toolbar>
  //     </AppBar>
  //   )
  // }

  render() {
    const { anchorEl, open } = this.state;
    const { user } = this.props;
    const { grow, avatar } = this.styles;

    return (
      <div className='header'>
        <button className='button' type='button' onClick={this.handleHomeClick}>HOME</button>
        <button className='button' type='button' onClick={this.handleMenuClick}>ACCOUNT</button>
         <AccountPopper
          open={open}
          anchorEl={anchorEl}
          onHandleMenuClick={this.handleMenuClick}
          onHandleClickAway={this.handleClickAway}
          transition/>
      </div>
  )
  }
}

const mapStateToProps = state => {
  return {
    user: state.auth.user
  }
};

export default connect(mapStateToProps)(Header);