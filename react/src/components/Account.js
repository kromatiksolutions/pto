import React, {Fragment} from 'react';
import {connect} from 'react-redux';

class Account extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    const { user } = this.props;
    return (
      <Fragment>
        <button onClick={this.props.handleTestClick}>Test Redux Action Button</button>
      </Fragment>
    )
  };
}

const mapStateToProps = state => ({
  user: state.auth.user
});

const mapDispatchToProps = dispatch => ({
  handleTestClick: () => dispatch(testAction())
});

export default connect(mapStateToProps, mapDispatchToProps)(Account);

function testAction() {
  return {
    type: 'TESTACTION',
    meta: {
        type: 'API'
    }
  }
}