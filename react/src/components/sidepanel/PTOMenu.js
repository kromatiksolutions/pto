import React from 'react';
import NewPTORequest from './NewPTORequest';
import {store} from "../../main";
import {APPROVALS, filter, MINE} from "../../actions/filter-actions";
import {getPTOForSeason} from "../../api/pto-api";

export default class PTOMenu extends React.Component {

  state = {
    requestFormOpen: false,
  };

  handleNewRequestClick = () => {
    console.log(this.props.ptos);
    this.setState({
      requestFormOpen: !this.state.requestFormOpen
    })};

  handleCloseNewRequest = () => {
    this.setState({ requestFormOpen: false });
  };

  handleMineFilter = () => {
    this.props.onHandleMineFilter();
  };

  handleApprovalsFilter = () => {
    this.props.onHandleApprovalsFilter();
  };

  handleDecrementSeason = () => {
    this.props.onHandleDecrementSeason();
  };

  handleIncrementSeason = () => {
    this.props.onHandleIncrementSeason();
  };

  render() {
    const { season } = this.props;
    const { requestFormOpen } = this.state;

    return (
    <div className='pto-menu'>
      <div>
        <button className='filter-button' type='button' onClick={this.handleMineFilter}>Mine</button>
        <button className='filter-button' type='button' onClick={this.handleApprovalsFilter}>Approvals</button>
      </div>
      <div className='filter-seasons'>
        <button type='button' onClick={this.handleDecrementSeason}>&lt;</button>
        <div className='season'>{`${season.start}-${season.end}`}</div>
        <button type='button' onClick={this.handleIncrementSeason}>&gt;</button>
      </div>
      <div className='new-pto'>
        <div className='re-days'>
          <p>9</p>
        </div>
        <button type='button' onClick={this.handleNewRequestClick}>New</button>
         <NewPTORequest
          open={requestFormOpen}
          onHandleClose={this.handleCloseNewRequest}/>
      </div>
    </div>
    )
  }
}