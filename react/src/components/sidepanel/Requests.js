import React from 'react';
import OngoingRequest from './OngoingRequest';

export default class Requests extends React.Component {

  render() {
    const { ptos } = this.props;

    return (
      <div className='requests'>
        <div>requests</div>
        <div>
          {
            ptos.map(pto => {
              return (<OngoingRequest key={pto.id} id={pto.id} fromDate={pto.fromDate} toDate={pto.toDate}/>);
            })
          }
        </div>
      </div>
    )
  }
}