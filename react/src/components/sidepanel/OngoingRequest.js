import React from 'react';
import Grid from '@material-ui/core/Grid';
import {Button} from '@material-ui/core';
import Clear from '@material-ui/icons/Clear'
import {deletePTO} from '../../api/pto-api';

export default class OngoingRequest extends React.Component {

  styles = {
    root: {
      margin: 10,
      padding: 10,
      backgroundColor: '#eeeeee'
    }
  };

  calculateNumberOfDays = (fromDate, toDate) => {
  };

  handleDeleteClick = () => {
    deletePTO(this.props.id);
  };

  render() {
    const { fromDate, toDate } = this.props;
    const { root } = this.styles;
    return (
      <Grid container direction='row' justify='space-between' style={root}>
        <Grid item>
          <Grid container direction='column' alignItems='flex-start'>
            <Grid item>from:</Grid>
            <Grid item>{fromDate}</Grid>
            <Grid item>to:</Grid>
            <Grid item>{toDate}</Grid>
          </Grid>
        </Grid>
        <Grid item><Grid container direction='column' alignItems='flex-start'>
          <Grid container direction='row' alignItems='flex-start'>
            {/*<Grid item>{numOfDays}</Grid>*/}
            <Grid item><Button onClick={this.handleDeleteClick}><Clear/></Button></Grid>
          </Grid>
        </Grid></Grid>
      </Grid>
    )
  }
}