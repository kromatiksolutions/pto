import React from 'react';
import Grid from '@material-ui/core/Grid';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Close from '@material-ui/icons/Close';
import AddCircle from '@material-ui/icons/AddCircle'
import ToggleButton from '@material-ui/lab/ToggleButton';
import ToggleButtonGroup from '@material-ui/lab/ToggleButtonGroup';
import {DatePicker, MuiPickersUtilsProvider} from 'material-ui-pickers';
import DateFnsUtils from '@date-io/date-fns';
import Chip from '@material-ui/core/Chip';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import InputLabel from '@material-ui/core/InputLabel';
import Input from '@material-ui/core/Input';
import {createPTO, getAllPTO} from '../../api/pto-api'

export default class NewPTORequest extends React.Component {

  state = {
    type: 'VACATION',
    from: null,
    to: null,
    approver: '',
    approvers: []
  };

  styles = {
    toggle: {
      flexGrow: 1,
      display: 'box',
      justifyContent: 'center'
    },
    form: {
      display: 'flex',
      flexDirection: 'column',
      margin: 'auto',
      width: 'fit-content'
    },
  };

  tryCreatePTO = ptoForm => {
    let params = {
      fromDate: ptoForm.from && ptoForm.from.toISOString().split('T')[0],
      toDate: ptoForm.to && ptoForm.to.toISOString().split('T')[0],
      type: ptoForm.type,
      approvers: ptoForm.approvers.map(a => {
        return {email: a}
      })
    };
    createPTO(params);
  };

  handleClose = () => this.props.onHandleClose();

  handleSubmit = () => {
    this.tryCreatePTO(this.state);
    this.props.onHandleClose();
    getAllPTO();
  };

  handleTypeToggle = (event, type) => this.setState({ type });

  handleFromDate = date => this.setState({ from: date });

  handleToDate = date => this.setState({ to: date });

  handleApprover = event => this.setState({ approver: event.target.value });

  handleAddApprover = () => this.setState(state => {
    const approvers = [...state.approvers];
    const approverToAdd = this.state.approver;
    if(approverToAdd && !approvers.includes(approverToAdd)) {
      approvers.push(approverToAdd);
    }
    return { approvers };
  });

  handleRemoveApprover = approver => () => {
    this.setState(state => {
      const approvers = [...state.approvers];
      const approverToRemove = approvers.indexOf(approver);
      approvers.splice(approverToRemove, 1);
      return { approvers };
    });
  };

  render() {
    const { open } = this.props;
    const { type, from, to, approver, approvers } = this.state;
    const { toggle, form } = this.styles;

    return (
      <Dialog
        open={open}
        onClose={this.handleClose}
        fullWidth
        maxWidth='sm'>

        <DialogTitle style={{background: '#eeeeee'}}>
          <Grid container spacing={0} direction='row' justify='space-between' alignItems='center'>
            <Grid item xs>
              Request Form
            </Grid>
            <Grid item>
              <IconButton onClick={this.handleClose}><Close/></IconButton>
            </Grid>
          </Grid>
        </DialogTitle>

        <DialogContent style={{margin: 20}}>
        <FormControl style={form}>
        <MuiPickersUtilsProvider utils={DateFnsUtils}>

          <ToggleButtonGroup value={type} exclusive onChange={this.handleTypeToggle} style={{display: 'flex'}}>
            <ToggleButton value='VACATION' style={toggle}>VACATION</ToggleButton>
            <ToggleButton value='SICK' style={toggle}>SICK</ToggleButton>
          </ToggleButtonGroup>
          <DatePicker
            margin='normal'
            label='From'
            value={from}
            onChange={this.handleFromDate}
          />
          <DatePicker
            margin='normal'
            label='To'
            value={to}
            onChange={this.handleToDate}
          />
          <FormControl>
            <InputLabel>Approver</InputLabel>
            <Input
              type='email'
              value={approver}
              onChange={this.handleApprover}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton onClick={this.handleAddApprover}><AddCircle/></IconButton>
                </InputAdornment>
              }
            />
          </FormControl>
          <div style={{display: 'flex', flexDirection: 'column', marginTop: 5}}>
            {
              approvers.map(approver => {
                return (
                  <Chip
                    key={approver}
                    label={approver}
                    onDelete={this.handleRemoveApprover(approver)}
                    style={{display: 'box', margin: 2}}
                  />
                )
              })
            }
          </div>

          <DialogActions>
            <Button onClick={this.handleClose} color="primary">Close</Button>
            <Button onClick={this.handleSubmit} color="primary">Save</Button>
          </DialogActions>

        </MuiPickersUtilsProvider>
        </FormControl>
        </DialogContent>
      </Dialog>
    )
  }
}