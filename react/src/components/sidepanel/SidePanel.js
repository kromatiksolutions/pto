import React, {Fragment} from 'react';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import Divider from '@material-ui/core/Divider';
import OngoingRequest from './OngoingRequest';
import NewPTORequest from './NewPTORequest';
import {connect} from 'react-redux';
import {getAllPTO, getPTOForSeason} from '../../api/pto-api'
import PTOMenu from './PTOMenu';
import Requests from './Requests';
import moment from 'moment';
import {store} from '../../main';
import {MINE, APPROVALS, filter, currentSeason, incrementSeason, decrementSeason} from '../../actions/filter-actions';

class SidePanel extends React.Component {

  state = {
    requestFormOpen: false
  };

  styles = {
    button: {
      margin: 10,
    },
    dates: {
      marginLeft: 10,
      marginRight: 10,
      background: '#eeeeee',
    },
    requests: {
      padding: 2,
      paddingLeft: 15,
      paddingRight: 15,
      background: '#eeeeee'
    }
  };

  componentDidMount() {
    store.dispatch(currentSeason(getCurrentSeason()));
  }

  componentDidUpdate(prevProps) {
    if (prevProps.filter.season !== this.props.filter.season) {
      getPTOForSeason(this.props.filter.season.start);
    }
  }

  handleNewRequestClick = () => {
    console.log(this.props.ptos);
    this.setState({
      requestFormOpen: !this.state.requestFormOpen
    })};

  handleCloseNewRequest = () => {
    this.setState({ requestFormOpen: false });
  };

  handleMineFilter = () => {
    store.dispatch(filter(MINE));
  };

  handleApprovalsFilter = () => {
    store.dispatch(filter(APPROVALS));
  };

  handleDecrementSeason = () => {
    store.dispatch(decrementSeason());
  };

  handleIncrementSeason = () => {
    store.dispatch(incrementSeason());
  };

  // render() {
  //   const { requestFormOpen } = this.state;
  //   const { ptos } = this.props;
  //   const { button, dates, requests } = this.styles;
  //
  //   return (
  //     <Fragment>
  //       <Grid container direction='column' spacing={0}>
  //         <Grid item>
  //           <Button variant='contained' style={button}>Mine</Button>
  //           <Button variant='contained' style={button}>Approvals</Button>
  //         </Grid>
  //         <Grid item>
  //           <Paper square style={dates}>
  //             <Grid container direction='row' alignItems='center' justify='space-between'>
  //               <Grid item><IconButton><KeyboardArrowLeft/></IconButton></Grid>
  //               <Grid item><Button>Dates</Button></Grid>
  //               <Grid item><IconButton><KeyboardArrowRight/></IconButton></Grid>
  //             </Grid>
  //           </Paper>
  //         </Grid>
  //         <Grid item>
  //           <Grid container direction='row' alignItems='center' justify='space-between'>
  //             <Grid item style={{flexGrow: 1, textAlign: 'center'}}>
  //               <Paper square style={dates}>Days Left
  //               </Paper>
  //             </Grid>
  //             <Grid item>
  //               <Button variant='contained' onClick={this.handleNewRequestClick} style={button}>New</Button>
  //               <NewPTORequest
  //                 open={requestFormOpen}
  //                 onHandleClose={this.handleCloseNewRequest}/>
  //             </Grid>
  //           </Grid>
  //         </Grid>
  //       </Grid>
  //       <Grid container direction='row' alignItems='center' justify='space-between'>
  //         <Grid item xs><Divider/></Grid>
  //         <Grid item><Paper style={requests}>requests</Paper></Grid>
  //         <Grid item xs><Divider/></Grid>
  //       </Grid>
  //       <Grid item xs style={{margin: 30}}>
  //         {
  //           ptos.map(pto => {
  //           return (<OngoingRequest key={pto.id} id={pto.id} fromDate={pto.fromDate} toDate={pto.toDate}/>);
  //           })
  //         }
  //       </Grid>
  //     </Fragment>
  //   )
  // }

  render() {
    const { ptos, filter } = this.props;

    return (
      <div className='side-panel'>
        <PTOMenu
          onHandleMineFilter={this.handleMineFilter}
          onHandleApprovalsFilter={this.handleApprovalsFilter}
          onHandleDecrementSeason={this.handleDecrementSeason}
          onHandleIncrementSeason={this.handleIncrementSeason}
          season={filter.season}/>
        <Requests
          ptos={ptos}/>
      </div>
    )
  }
}

const mapStateToProps = state => ({ptos: state.ptos, filter: state.filter});

export default connect(mapStateToProps)(SidePanel);

function getCurrentSeason() {
  let now = moment();
  let start = now.month() >= 7 ? now.add(1, 'y').year() : now.year();
  let end = start + 1;
  return {
    start: start,
    end: end
  }
}