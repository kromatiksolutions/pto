import React, {Fragment} from 'react';
import Grid from '@material-ui/core/Grid';
import SidePanel from './sidepanel/SidePanel';
import PTOs from './PTOs';

export default class Home extends React.Component {

  style = {
    sideBorder: {
      borderRightStyle: 'solid',
      borderRightWidth: 1,
      borderColor: 'lightGray',
      height: '90vh'
    }
  };

  constructor(props) {
    super(props);
  }

  // render() {
  //   const { sideBorder } = this.style;
  //   return (
  //     <Fragment>
  //       <Grid container spacing={0} direction='row' justify='space-between' alignItems='stretch'>
  //         <Grid item style={sideBorder}>
  //           <SideMenu/>
  //         </Grid>
  //         <Grid item xs>
  //           <PTOs/>
  //         </Grid>
  //       </Grid>
  //     </Fragment>
  //   )
  // }

  render() {
    const { sideBorder } = this.style;
    return (
      <div className='home'>
        <SidePanel/>
        <PTOs/>
      </div>
    )
  }
}
