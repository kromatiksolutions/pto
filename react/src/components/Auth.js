import React from 'react';
import {Redirect} from 'react-router-dom';
import tryAuthenticate from '../auth/tryAuthenticate';

export default class Auth extends React.Component {

  constructor(props) {
    super(props);
  }

  componentDidMount() {
    tryAuthenticate();
  }

  render() {
    return <Redirect to='/'/>
  };
}