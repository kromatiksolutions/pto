import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from 'react-redux';
import configureStore, {history} from './configure-store';
import RootRouter from './routers/root-router';
import CssBaseline from '@material-ui/core/CssBaseline';
import './styles/styles.scss';

export const store = configureStore();

const Root = (
  <Provider store={store}>
    <CssBaseline />
    <RootRouter history={history}/>
  </Provider>
);

ReactDOM.render(Root, document.getElementById('main'));