const Joi = require('joi');
const log4js = require('log4js');
const userSchema = require('../joi-validation/user-validation-schema');

const logger = log4js.getLogger();

const validationOptions = {
  abortEarly: false, // display all errors, default (true) will fail after the first validation error
  allowUnknown: true,
  stripUnknown: true,
};
function buildErrorMessage() {
  return {
    status: 'Failed!!!',
    error: 'Invalid request data.',
  };
}
const validateRequest = async (req, res, next) => {
  try {
    logger.debug('Validating Request');
    req.body = await Joi.validate(req.body, userSchema, validationOptions);

    return next();
  } catch (e) {
    return res.status(400).send(buildErrorMessage());
  }
};
module.exports = {
  validateRequest,
};
