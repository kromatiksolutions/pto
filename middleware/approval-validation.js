const Joi = require('joi');
const approvalRequestSchema = require('../joi-validation/approval-validation-schema');

const validationOptions = {
  abortEarly: false,
  allowUnknown: false,
};

const validateRequest = async (req, res, next) => {
  try {
    req.body = await Joi.validate(
      req.body,
      approvalRequestSchema,
      validationOptions
    );

    return next();
  } catch (e) {
    return res.status(400).send('Bad Request');
  }
};
module.exports = {
  validateRequest,
};
