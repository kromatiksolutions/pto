const Joi = require('joi');
const { Types } = require('mongoose');
const ptoRequestSchema = require('../joi-validation/pto-validation-schema');

const validationOptions = {
  abortEarly: false, // display all errors, default (true) will fail after the first validation error
  allowUnknown: true,
  stripUnknown: true,
};

const buildErrorMessage = () => {
  return {
    status: 'Failed!!!',
    error: 'Invalid request data.',
  };
};

const validateId = (req, res, next) => {
  if (!Types.ObjectId.isValid(req.params.id)) {
    return res.status(404).send('PTO not found');
  }

  return next();
};

const validatePostRequest = async (req, res, next) => {
  try {
    req.body = await Joi.validate(
      req.body,
      ptoRequestSchema,
      validationOptions
    );

    return next();
  } catch (e) {
    return res.status(400).send(buildErrorMessage());
  }
};

const calculateFreeDays = async ptos => {
  let freeDays = 21;

  ptos
    .filter(pto => pto.status !== 'rejected')
    .forEach(pto => {
      freeDays -= JSON.parse(JSON.stringify(pto)).workDaysCount;
    });

  if (freeDays > 21) {
    throw new Error('Bad Request:No free days left');
  }

  return freeDays;
};

const validatePutRequest = async (req, res, next) => {
  try {
    ptoRequestSchema.optionalKeys('fromDate', 'toDate', 'type').options({
      // makes the keys from required to optional
      noDefaults: true, // to prevent from triggering the default values if not provided
    });

    req.body = await Joi.validate(
      req.body,
      ptoRequestSchema,
      validationOptions
    );

    return next();
  } catch (e) {
    return res.status(400).send(buildErrorMessage());
  }
};

module.exports = {
  validatePostRequest,
  validatePutRequest,
  validateId,
  calculateFreeDays,
};
