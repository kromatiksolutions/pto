const express = require('express');
const passport = require('passport');
const Auth0Strategy = require('passport-auth0');
const config = require('config');
const log4js = require('log4js');
const UserController = require('../controllers/user-controller');

const AUTH0_APP_DOMAIN = config.get('auth0.domain');
const AUTH0_CLIENT_ID = config.get('auth0.client');
const AUTH0_SECRET = config.get('auth0.secret');
const AUTH0_CALLBACK = config.get('auth0.callbackUrl');
const AUTH0_LOGOUT_TO_RETURN = config.get('auth0.logoutToReturnUrl');

const authRouter = express.Router();
const logger = log4js.getLogger();

/**
 * Auth0 Strategy that we will configure
 * in passport and use it for authentication purposes.
 */
const auth0Strategy = new Auth0Strategy(
  {
    domain: AUTH0_APP_DOMAIN,
    clientID: AUTH0_CLIENT_ID,
    clientSecret: AUTH0_SECRET,
    callbackURL: AUTH0_CALLBACK || 'https://localhost:3000/callback',
  },
  (_accessToken, _refreshToken, _extraParams, profile, done) => {
    // accessToken is the token to call Auth0 API
    // (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user
    return done(null, profile);
  }
);

/**
 * Middleware that will be checking if the current request
 * has authenticated user associated with it. If not that it
 * will result with 401. Caller must know how to handle 401s.
 */
const isAuth = (req, res, next) => {
  if (req.user) {
    return next();
  }

  req.session.returnTo = req.originalUrl;

  return res
    .status(401)
    .send('User can not be authenticated/authorized for this call');
};

/**
 * Called to initiate the login process with Auth0
 */
authRouter.get(
  '/login',
  passport.authenticate('auth0', {
    scope: 'openid email profile',
  }),
  (_req, res) => {
    res.redirect('/');
  }
);

const storeUser = async user => {
  try {
    logger.debug('Storing the user in the DB');
    const profile = await UserController.findUserByGoogleId(user.id);

    if (profile) {
      logger.debug('User Found, returning...');

      return profile;
    }

    return await UserController.createUser(user);
  } catch (e) {
    console.error('Error occurred during saving the user');
    throw new Error(e);
  }
};

/**
 * After successful authentication validate and store the user in the database
 * Then perform the final stage of authentication and redirect to '/'
 */
authRouter.get('/callback', (req, res, next) => {
  passport.authenticate('auth0', (err, user, _info) => {
    if (err) {
      return next(err);
    }
    if (!user) {
      return res.redirect('/login');
    }

    req.logIn(user, async err => {
      if (err) {
        return next(err);
      }

      await storeUser(user);

      return res.redirect('/authenticated');
    });
  })(req, res, next);
});

/**
 * Kills the session which in turn logs out the user
 */
authRouter.get('/logout', (req, res) => {
  req.logout();
  if (req.session) {
    req.session.destroy(err => {
      if (err) {
        logger.debug(err);
      }
      logger.debug('Destroyed the user session on Auth0 endpoint');
      res.redirect(
        `https://${AUTH0_APP_DOMAIN}/v2/logout?client_id=${AUTH0_CLIENT_ID}&returnTo=${AUTH0_LOGOUT_TO_RETURN}`
      );
    });
  } else {
    res.redirect('/');
  }
});

/**
 * Frontend can call this to fetch user data of authenticated user.
 * Will trigger authentication flow if there is no logged in user.
 */
authRouter.get('/auth', isAuth, (req, res) => {
  res.status(200).send(JSON.stringify(req.user));
});

module.exports = {
  auth0Strategy,
  authRouter,
  isAuth,
};
