## Project Structure

### Folder structure

**Root** folder contains node server application. 

`/angular` folder contains the angular app that is used as frontend as part of this project. The build artifacts of the angular app will end up in folder `/angular/dist` and they are being statically server by the node app (do not put sensitive data id this folder).

### Build (backend or prod deployment)

For backend development or production deployment run the command `npm run build` to generate pull all dependencies and generate all the artifacts required to run the app. After this command run `npm start` and the application will start and serve the frontend at context path `/`

### Build (frontend)

For working with the frontend check the README.md in `/angular`

## Managing Configuration 

We are using node-config module for managing project configuration.

Default project configuration would go into `./config/default.json`.

Each environment is expected to have their own config file. Example product would have `./config/production.json`. Production needs to only override configuration from `./config/default.json`. To start in production mode use the environment variable `NODE_ENV=production` to start the server.

Some settings will not be internal, but external due to security. This kind of settings are password, secret, keys. The definition of what can be enviroment variable is in file `/config/custom-environment-variables.json`. If you configure a variable that exist in this file it will override any property from the configuration.

## Node Express template project

This project is based on a GitLab [Project Template](https://docs.gitlab.com/ee/gitlab-basics/create-project.html).

Improvements can be proposed in the [original project](https://gitlab.com/gitlab-org/project-templates/express).

## CI/CD with Auto DevOps

This template is compatible with [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/).

If Auto DevOps is not already enabled for this project, you can [turn it on](https://docs.gitlab.com/ee/topics/autodevops/#enabling-auto-devops) in the project settings.

