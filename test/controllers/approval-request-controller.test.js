const sinon = require('sinon');
const { expect } = require('expect.js');
const {
  approvedPto,
  rejectedPto,
  testPto,
  updateOptions,
  testApproval,
} = require('../fakedb/approvals-db');
const PtoRequest = require('../../mongo/model/pto-request');
const approvalRequestController = require('../../controllers/approval-request-controller');

describe('approvalControllers', () => {
  let sandbox;
  beforeEach(() => {
    sandbox = sinon.createSandbox();
  });
  afterEach(() => {
    sandbox.restore();
  });

  describe('getAllApprovals', () => {
    it('should return an array', async () => {
      sandbox.stub(PtoRequest, 'find').returns([]);
      const ptos = await approvalRequestController.getAllApprovals();
      expect(ptos.length).toBeGreaterThanOrEqual(0);
    });
  });
  describe('getApprovalRequest', () => {
    it('should return one PTO when valid arguments are provided', async () => {
      sandbox
        .stub(PtoRequest, 'findOne')
        .withArgs({
          _id: testPto._id,
          'approvers.email': testPto.approvers[0].email,
        })
        .returns(testPto);
      const pto = await approvalRequestController.getApprovalRequest(
        testPto._id,
        testPto.approvers[0].email
      );
      expect(PtoRequest.findOne.calledOnce).toBe(true);
      expect(pto).toMatchObject(testPto);
    });
    it('should throw a error when a non existent id is provided', async () => {
      try {
        sandbox
          .stub(PtoRequest, 'findOne')
          .withArgs({
            _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
            'approvers.email': testPto.approvers[0].email,
          })
          .returns(null);
        await approvalRequestController.getApprovalRequest(
          'aaaaaaaaaaaaaaaaaaaaaaaa',
          testPto.approvers[0].email
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
    it('should throw a error when no arguments are provided', async () => {
      // works for null
      try {
        sandbox
          .stub(PtoRequest, 'findOne')
          .withArgs({
            _id: undefined,
            'approvers.email': undefined,
          })
          .returns(null);
        await approvalRequestController.getApprovalRequest(
          undefined,
          undefined
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
  describe('approveRequest', () => {
    it('should return a approvedPto when valid request is sent', async () => {
      sandbox
        .stub(PtoRequest, 'findOneAndUpdate')
        .withArgs(
          {
            _id: testPto._id,
            'approvers.email': testPto.approvers[0].email,
          },
          { $set: { status: 'approved', comment: testApproval.comment } },
          updateOptions
        )
        .returns(approvedPto);
      const pto = await approvalRequestController.approvePtoRequest(
        testApproval,
        testPto._id,
        testPto.approvers[0].email
      );
      expect(pto).toEqual(approvedPto);
      expect(pto.status).toBe('approved');
      expect(typeof pto.comment).toBe('string');
    });
    it('should throw an error when no PTO is found', async () => {
      try {
        sandbox
          .stub(PtoRequest, 'findOneAndUpdate')
          .withArgs(
            {
              _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
              'approvers.email': testPto.approvers[0].email,
            },
            { $set: { status: 'approved', comment: testApproval.comment } },
            updateOptions
          )
          .returns(null);
        await approvalRequestController.approvePtoRequest(
          testApproval,
          'aaaaaaaaaaaaaaaaaaaaaaaa',
          testPto.approvers[0].email
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
    it('should throw an error when invalid arguments are provided', async () => {
      try {
        sandbox.stub(PtoRequest, 'findOneAndUpdate').returns(null);
        await approvalRequestController.approvePtoRequest(
          undefined,
          undefined,
          undefined
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
  describe('rejectPto', () => {
    it('should return a rejectedPto when valid request is sent', async () => {
      sandbox
        .stub(PtoRequest, 'findOneAndUpdate')
        .withArgs(
          {
            _id: testPto._id,
            'approvers.email': testPto.approvers[0].email,
          },
          { $set: { status: 'rejected', comment: testApproval.comment } },
          updateOptions
        )
        .returns(rejectedPto);

      const pto = await approvalRequestController.rejectPtoRequest(
        testApproval,
        testPto._id,
        testPto.approvers[0].email
      );
      expect(pto).toEqual(rejectedPto);
      expect(pto.status).toBe('rejected');
      expect(typeof pto.comment).toBe('string');
    });
    it('should throw an error when no PTO is found', async () => {
      try {
        sandbox
          .stub(PtoRequest, 'findOneAndUpdate')
          .withArgs(
            {
              _id: 'aaaaaaaaaaaaaaaaaaaaaaaa',
              'approvers.email': testPto.approvers[0].email,
            },
            { $set: { status: 'rejected', comment: testApproval.comment } },
            updateOptions
          )
          .returns(null);
        await approvalRequestController.rejectPtoRequest(
          testApproval,
          'aaaaaaaaaaaaaaaaaaaaaaaa',
          testPto.approvers[0].email
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
    it('should throw an error when invalid arguments are provided', async () => {
      try {
        sandbox.stub(PtoRequest, 'findOneAndUpdate').return(null);
        await approvalRequestController.rejectPtoRequest(
          undefined,
          undefined,
          undefined
        );
      } catch (e) {
        expect(e).toBeInstanceOf(Error);
      }
    });
  });
});
