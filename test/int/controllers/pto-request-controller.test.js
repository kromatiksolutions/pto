const expect = require('expect.js');
const ptoRequestController = require('../../../controllers/pto-request-controller');
require('../../../mongo/setup/mongoose-setup')();
const PtoRequest = require('../../../mongo/model/pto-request');

const mongoId = '5ce2a8570ecbf04eac5cb37e';

describe('PTO Controllers tests', async () => {
  it('Should find empty array of PTO requests.', async () => {
    const ptoRequests = await ptoRequestController.getAllPtoRequests();
    expect(ptoRequests).to.not.be(undefined);
    expect(ptoRequests).to.be.an(Array);
    expect(ptoRequests.length).to.equal(0);
  });

  describe('getAllPtoRequests single entity', () => {
    before(async () => {
      await new PtoRequest({
        fromDate: new Date(),
        toDate: new Date(),
        type: 'Vacation',
        approvers: [{ email: 'test2@test.com' }],
        userEmail: 'test.test@gmail.com',
        userId: mongoId,
      }).save();
    });

    after(async () => {
      await PtoRequest.deleteOne({ type: 'Vacation' });
    });

    it('Should find array of all PTO requests.', async () => {
      const ptoRequests = await ptoRequestController.getAllPtoRequests();
      expect(ptoRequests).to.be.an(Array);
      expect(ptoRequests.length).to.equal(1);
      expect(ptoRequests[0]).to.have.property('fromDate');
      expect(ptoRequests[0]).to.have.property('toDate');
      expect(ptoRequests[0]).to.have.property('type');
      expect(ptoRequests[0]).to.have.property('approvers');
      expect(ptoRequests[0].approvers).to.be.an('array');
      expect(ptoRequests[0].approvers).to.have.length(1);
      expect(ptoRequests[0].approvers[0]).to.have.property('email');
      expect(ptoRequests[0].approvers[0].email).to.eql('test2@test.com');
      expect(ptoRequests[0].userEmail).to.eql('test.test@gmail.com');
      expect(ptoRequests[0].userId).to.equal(mongoId);
    });
  });

  describe('getAllPtoRequests multiple entities', () => {
    before(async () => {
      await new PtoRequest({
        fromDate: new Date(),
        toDate: new Date(),
        type: 'Vacation',
        approvers: [{ email: 'test2@test.com' }],
        userEmail: 'test.test@gmail.com',
        userId: mongoId,
      }).save();

      await new PtoRequest({
        fromDate: new Date(),
        toDate: new Date(),
        type: 'Sick',
        approvers: [{ email: 'test3@test.com' }],
        userEmail: 'test.test@gmail.com',
        userId: mongoId,
      }).save();

      await new PtoRequest({
        fromDate: new Date(),
        toDate: new Date(),
        type: 'Sick',
        approvers: [{ email: 'test4@test.com' }],
        userEmail: 'test.test@gmail.com',
        userId: mongoId,
      }).save();
    });

    after(async () => {
      await PtoRequest.deleteOne({ type: 'Vacation' });
      await PtoRequest.deleteMany({ type: 'Sick' });
    });

    it('Should find array of all PTO requests.', async () => {
      const ptoRequests = await ptoRequestController.getAllPtoRequests();
      expect(ptoRequests).to.be.an(Array);
      expect(ptoRequests.length).to.equal(3);
      expect(ptoRequests[0].type).to.eql('Vacation');
    });
  });
});
