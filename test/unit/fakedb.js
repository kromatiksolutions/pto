const updateOptions = {
    new: true,
};
const testApproval =  {
    comment: 'A comment'
};
const testPto = {
    _id:'123214124412412412121212',
    fromDate: new Date(),
    toDate: new Date(),
    type: 'Vacation',
    approvers: [{
        email: 'test@hotmail.com'
    }],
    status: 'pending',
    comment: ''
};
const approvedPto = {
    _id:'123214124412412412121212',
    fromDate: new Date(),
    toDate: new Date(),
    type: 'Vacation',
    approvers: [{
        email: 'test@hotmail.com'
    }],
    status: 'approved',
    comment: 'Random'
};
const rejectedPto = {
    _id: '1232141244124124',
    fromDate: new Date(),
    toDate: new Date(),
    type: 'Vacation',
    approvers: [{
        email: 'test@hotmail.com'
    }],
    status: 'rejected',
    comment: 'Random'
};
const user = {
    _id: "123214124412412411111111",
    googleId: "123123123",
    userName: "User Name",
    emailAddress: "test@hotmail.com",
};
module.exports = {
    approvedPto,
    rejectedPto,
    testPto,
    updateOptions,
    testApproval,
    user
};