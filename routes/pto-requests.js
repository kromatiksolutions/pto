const express = require('express');
const moment = require('moment-business-days');
const jsLogger = require('log4js');
const ptoRequestController = require('../controllers/pto-request-controller');
const userController = require('../controllers/user-controller');
const {
  validatePostRequest,
  validatePutRequest,
  validateId,
  calculateFreeDays,
} = require('../middleware/pto-validation');
const { isAuth } = require('../middleware/auth0');

const router = express.Router();
const logger = jsLogger.getLogger();

const { NotFoundError } = require('../errors/errors');

const options = {
  // default options
  skip: 0,
  limit: 10,
  sort: {
    fromDate: 'asc',
  },
};

const buildMatchingCriteria = (req, existingUser) => {
  const year = parseInt(req.query.year);
  const start = moment(`01-07-${year}`, 'DD-MM-YYYY');
  const end = moment(`30-06-${year + 1}`, 'DD-MM-YYYY');
  const match = {
    $or: [
      {
        fromDate: { $gte: start },
        toDate: { $lte: end },
      },
      {
        fromDate: { $gte: start, $lte: end },
        toDate: { $gte: end },
      },
      {
        fromDate: { $lte: start },
        toDate: { $lte: end, $gte: start },
      },
    ],
  };

  match.userId = existingUser._id;

  if (req.query.status) {
    match.status = req.query.status;
  }

  if (req.query.limit) {
    options.limit = parseInt(req.query.limit);
  }

  if (req.query.page) {
    options.skip = options.limit * (parseInt(req.query.page) - 1);
  }

  if (req.query.sortBy) {
    // sortBy=updatedAt_asc OR sortBy=fromDate_desc
    const split = req.query.sortBy.split('_');
    options.sort = {};
    options.sort[split[0]] = split[1];
  }

  return { match, year };
};

const getExistingUserByCallerId = async userId => {
  const existingUser = await userController.findUserByGoogleId(userId);

  if (!existingUser) {
    logger.error('Unsupported operation');
    throw new Error('Unsupported operation!');
  }

  return existingUser;
};

const fetchAllPtoRequests = async (req, res, _next) => {
  try {
    if (!req.query.year) {
      throw new Error('Year must be defined');
    }

    const existingUser = await getExistingUserByCallerId(req.user.id);

    const { match, year } = buildMatchingCriteria(req, existingUser);

    const ptos = await ptoRequestController.getAllPtoRequests(
      match,
      options,
      year
    );

    return res.status(200).json(ptos);
  } catch (e) {
    return res.status(400).send('Bad Request');
  }
};

const getPtoRequest = async (req, res, _next) => {
  try {
    const existingUser = await getExistingUserByCallerId(req.user.id);

    const pto = await ptoRequestController.getPtoRequest(
      req.params.id,
      existingUser._id.toString()
    );

    return res.status(200).json(pto);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send('Bad Request');
  }
};

const updatePtoRequest = async (req, res, _next) => {
  try {
    const existingUser = await getExistingUserByCallerId(req.user.id);

    const pto = await ptoRequestController.updatePtoRequest(
      req.params.id,
      req.body,
      existingUser._id.toString()
    );

    return res.status(200).json(pto);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send('Bad Request');
  }
};

const deletePtoRequest = async (req, res, _next) => {
  try {
    const existingUser = await getExistingUserByCallerId(req.user.id);

    const pto = await ptoRequestController.deletePtoRequest(
      req.params.id,
      existingUser._id.toString()
    );

    return res.status(200).json(pto);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send('Bad Request');
  }
};

const savePtoRequest = async (req, res, _next) => {
  try {
    const existingUser = await getExistingUserByCallerId(req.user.id);
    const pto = await ptoRequestController.savePtoRequest(
      req.body,
      existingUser._id.toString(),
      existingUser.emailAddress
    );

    return res.status(200).json(pto);
  } catch (e) {
    return res.status(400).send('Bad Request');
  }
};

const getRemainingFreeDays = async (req, res, _next) => {
  try {
    const existingUser = await getExistingUserByCallerId(req.user.id);

    if (!req.query.year) {
      throw new Error('Year must be defined');
    }

    const { match, year } = buildMatchingCriteria(req, existingUser);

    const ptos = await ptoRequestController.getAllPtoRequests(
      match,
      options,
      year
    );

    const freeDaysCount = await calculateFreeDays(ptos);

    return res.status(200).json({ freeDaysCount });
  } catch (e) {
    return res.status(400).send('Bad Request');
  }
};

router.get('/', isAuth, fetchAllPtoRequests);
router.get('/remaining', isAuth, getRemainingFreeDays);
router.get('/:id', isAuth, validateId, getPtoRequest);
router.post('/', isAuth, validatePostRequest, savePtoRequest);
router.put('/:id', isAuth, validateId, validatePutRequest, updatePtoRequest);
router.delete('/:id', isAuth, validateId, deletePtoRequest);

module.exports = router;
