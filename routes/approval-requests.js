const express = require('express');

const router = express.Router();

const moment = require('moment');

const approvalRequestController = require('../controllers/approval-request-controller');

const { isAuth } = require('../middleware/auth0');
const { validateRequest } = require('../middleware/approval-validation');
const { validateId } = require('../middleware/pto-validation');

const { NotFoundError } = require('../errors/errors');

async function getAllApprovals(req, res, _next) {
  const { user } = req;
  const email = user.emails[0].value;
  const options = {
    // default options
    skip: 0,
    limit: 10,
    sort: {
      fromDate: 'asc',
    },
  };
  let year = moment().year();
  if (req.query.year) {
    year = parseInt(req.query.year);
  }
  const start = moment(`01-07-${year}`, 'DD-MM-YYYY');
  const end = moment(`30-06-${year + 1}`, 'DD-MM-YYYY');
  const match = {
    'approvers.email': email,
    $or: [
      {
        fromDate: { $gte: start },
        toDate: { $lte: end },
      },
      {
        fromDate: { $gte: start, $lte: end },
        toDate: { $gte: end },
      },
      {
        fromDate: { $lte: start },
        toDate: { $lte: end, $gte: start },
      },
    ],
  };
  if (req.query.status) {
    match.status = req.query.status;
  }
  if (req.query.limit) {
    options.limit = parseInt(req.query.limit);
  }
  if (req.query.page) {
    options.skip = options.limit * (parseInt(req.query.page) - 1);
  }
  if (req.query.sortBy) {
    // sortBy=updatedAt_asc OR sortBy=fromDate_desc
    const split = req.query.sortBy.split('_');
    options.sort = {};
    options.sort[split[0]] = split[1];
  }
  try {
    const approvals = await approvalRequestController.getAllApprovals(
      match,
      options,
      year
    );

    return res.status(200).json(approvals);
  } catch (e) {
    return res.status(400).send(e.message);
  }
}
async function getApprovalRequest(req, res, _next) {
  try {
    const { user } = req;
    const email = user.emails[0].value;
    const approval = await approvalRequestController.getApprovalRequest(
      req.params.id,
      email
    );

    return res.status(200).json(approval);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send(e.message);
  }
}
async function approvePtoRequest(req, res, _next) {
  try {
    const { id } = req.params;
    const { user } = req;
    const email = user.emails[0].value;
    const ptoResponse = await approvalRequestController.approvePtoRequest(
      req.body,
      id,
      email
    );

    return res.status(200).json(ptoResponse);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send(e.message);
  }
}

const rejectPtoRequest = async (req, res, _next) => {
  try {
    const { id } = req.params;
    const { user } = req;
    const email = user.emails[0].value;
    const ptoResponse = await approvalRequestController.rejectPtoRequest(
      req.body,
      id,
      email
    );

    return res.status(200).json(ptoResponse);
  } catch (e) {
    if (e instanceof NotFoundError) {
      return res.status(404).send(e.message);
    }

    return res.status(400).send(e.message);
  }
};

router.get('/', isAuth, getAllApprovals);
router.get('/:id', isAuth, validateId, getApprovalRequest);
router.post(
  '/:id/approve',
  isAuth,
  validateId,
  validateRequest,
  approvePtoRequest
);
router.post(
  '/:id/reject',
  isAuth,
  validateId,
  validateRequest,
  rejectPtoRequest
);
router.post('/:id/*', (_req, res) => {
  return res.status(400).send('Bad Request');
});

module.exports = router;
