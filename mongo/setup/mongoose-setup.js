const mongoose = require('mongoose');
const config = require('config');

const connection = config.get('mongo.connection');
const jsLogger = require('log4js');

const logger = jsLogger.getLogger('mongo');

module.exports = () => {
  mongoose.connect(connection, { useNewUrlParser: true }).then(
    () => {
      logger.info(`Successfully connected to mongoDB: ${connection}`);
    },
    err => {
      logger.error(
        `MongoDB connection error, please check your configuration ${err} string: ${connection}`
      );
    }
  );
};
