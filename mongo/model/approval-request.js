const mongoose = require('mongoose');
const approvalRequestSchema = require('./schema/approval-request-schema');

const Approval= mongoose.model('Approval',approvalRequestSchema);

module.exports = Approval;