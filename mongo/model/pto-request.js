const mongoose = require('mongoose');
const ptoRequestSchema = require('./schema/pto-request-schema');

const PtoRequest = mongoose.model('PtoRequest', ptoRequestSchema);

module.exports = PtoRequest;
