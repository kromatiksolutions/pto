const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const approvalRequestSchema = new Schema({
    comment:{
        type:String,
        required:true
    }
});

module.exports = approvalRequestSchema;
