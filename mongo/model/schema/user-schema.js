const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const userSchema = new Schema({
  googleId: {
    type: String,
    required: true
  },
  userName: {
    type: String,
    required: true
  },
  emailAddress: {
    type: String,
    required: true
  },
  profileImageUrl: {
    type: String,
    required: false
  }
});

module.exports = userSchema;
