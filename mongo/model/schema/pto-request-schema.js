/* eslint-disable func-names */
const mongoose = require('mongoose');
const { isEmail } = require('validator');
const moment = require('moment-business-days');

moment.updateLocale('en', {
  workingWeekdays: [1, 2, 3, 4, 5], // mon-fri
});

const { Schema } = mongoose;

const ptoRequestSchema = new Schema({
  userId: {
    type: String,
    required: true,
  },
  userEmail: {
    type: String,
    required: true,
    trim: true,
    minLength: 1,
    unique: false,
    validate: {
      validator: value => isEmail(value),
      message: '{VALUE} is not a valid email',
    },
  },
  fromDate: {
    type: Date,
    required: true,
  },
  toDate: {
    type: Date,
    required: true,
  },
  type: {
    type: String,
    required: true,
  },
  approvers: [
    {
      email: {
        type: String,
        required: true,
        trim: true,
        minLength: 1,
        unique: false,
        validate: {
          validator: value => isEmail(value),
          message: '{VALUE} is not a valid email',
        },
      },
    },
  ],
  status: {
    type: String,
    enum: ['pending', 'rejected', 'approved'],
    default: 'pending',
    required: false,
  },
  comment: {
    type: String,
    default: '',
    required: false,
    trim: true,
  },
});

let year = moment().year();
ptoRequestSchema.statics.findBySeason = async function(match, options, season) {
  year = season;

  return this.find(match, null, options);
};

ptoRequestSchema.methods.toJSON = function() {
  // define what we want to send back to the client
  const ptoObject = this.toObject();
  const start = moment(`01-07-${year}`, 'DD-MM-YYYY');
  const end = moment(`30-06-${year + 1}`, 'DD-MM-YYYY');

  if (ptoObject.toDate >= end && ptoObject.fromDate <= end) {
    ptoObject.toDate = moment(end).endOf('day');
  } else if (ptoObject.fromDate <= start && ptoObject.toDate >= start) {
    ptoObject.fromDate = moment(start).startOf('day');
  }

  ptoObject.workDaysCount = moment(ptoObject.toDate).businessDiff(
    moment(ptoObject.fromDate).startOf('day')
  );

  if (
    ptoObject.workDaysCount === 0 &&
    moment(ptoObject.toDate).weekday() !== 0 &&
    moment(ptoObject.toDate).weekday() !== 6
  ) {
    ptoObject.workDaysCount = 1;
  }

  return ptoObject;
};

module.exports = ptoRequestSchema;
