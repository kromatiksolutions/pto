const createError = require('http-errors');
const path = require('path');
const logger = require('morgan');
const config = require('config');
const cookieParser = require('cookie-parser');
const express = require('express');
const session = require('express-session');
const passport = require('passport');
const cors = require('cors');
const log4js = require('log4js');
const auth = require('./middleware/auth0');
const ptoRequestRouter = require('./routes/pto-requests');
const approvalsRequestRouter = require('./routes/approval-requests');

log4js.configure({
  appenders: { out: { type: 'stdout' } },
  categories: {
    default: {
      appenders: ['out'],
      level: config.get('log4js.categories.default.level'),
    },
    mongo: {
      appenders: ['out'],
      level: config.get('log4js.categories.mongo.level'),
    },
  },
});

require('./mongo/setup/mongoose-setup')();

const app = express();

app.use(logger('dev'));
app.options('*', cors());
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use('/authenticated', express.static(path.join(__dirname, 'public')));

// config express-session
const sessionOptions = {
  secret: config.get('server.sessionSecret'),
  cookie: {
    secure: false,
  },
  resave: false,
  saveUninitialized: true,
};
app.use(session(sessionOptions));

passport.use(auth.auth0Strategy);
app.use(passport.initialize());
app.use(passport.session());

passport.serializeUser((user, done) => {
  done(null, user);
});

passport.deserializeUser((user, done) => {
  done(null, user);
});

app.use('/', auth.authRouter);
app.use('/api/v1/ptos', ptoRequestRouter);
app.use('/api/v1/approvals', approvalsRequestRouter);
// if non of the previous URLs are hit then start the angular app
app.use((_req, res, _next) => {
  res.sendFile(path.join(__dirname, 'public', 'index.html'));
});

// catch 404 and forward to error handler
app.use((_req, _res, next) => {
  next(createError(404));
});

// error handler
app.use((err, req, res, _next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
